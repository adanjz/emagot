<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('items', 'ItemsController');

Route::resource('legajos', 'LegajosController');

Route::resource('tipoTareas', 'TipoTareaController');

Route::resource('tareas', 'TareasController');

Route::get('tareas/filtrar/{status}/{lote}', 'TareasController@index')->name('status');

Route::post('buscarTareas','TareasController@buscarTareas');

Route::resource('stocks', 'StockController');

Route::resource('legajoGps', 'LegajoGpsController');

Route::resource('anomaliases', 'AnomaliasController');

Route::resource('clientes', 'ClientesController');

Route::resource('users', 'UsersController');

Route::get('importarTareas','TareasController@loadTasks');
Route::post('saveRuta','TareasController@saveRuta');

Route::get('modificarRutas','TareasController@modificarRutas');

Route::post('saveRoutes/rutas','TareasController@saveRutas');

Route::post('saveRoutesPolygon/rutas','TareasController@guardarRutasPoly');

Route::resource('inputFormats', 'InputFormatsController');

Route::resource('taskTypes', 'TaskTypesController');

Route::resource('subTasks', 'SubTasksController');

Route::resource('materialTypes', 'MaterialTypesController');

Route::resource('flotillas', 'FlotillasController');

Route::resource('flotillasUsuarios', 'FlotillasUsuariosController');


Route::get('/api/token', 'Api\TokenController@index');

Route::post('/api/user/register', 'Api\UserController@register');
Route::post('/api/user/login', 'Api\UserController@login');
Route::post('/api/user/token_login', 'Api\UserController@token_login');
Route::get('/api/user/logout', 'Api\UserController@logout');
Route::get('/api/fcm/register', 'Api\UserController@fcm_register');
Route::get('/api/user_location', 'Api\LocationController@register_location');
Route::get('/api/user_medidores/{user_id}','Api\UserController@getMedidores');
Route::get('/api/get_face_ids', 'Api\UserController@get_face_ids');
Route::get('/api/download/{file_name}', 'Api\UserController@download_face_ids');

Route::resource('/api/task', 'Api\TaskController');
Route::get('/api/my_task/{id}', 'Api\TaskController@index');
Route::post('/api/task/start', 'Api\TaskController@start_task');
Route::post('/api/task/finish', 'Api\TaskController@finish_task');
Route::post('/api/task/photo', 'Api\TaskController@post_task_photo');
Route::post('/api/task/set_anomaly', 'Api\TaskController@set_anomaly');
Route::get('/api/test', 'Api\TaskController@test');

Route::get('/api/stock/{id}', 'Api\StockController@index');

Route::get('/api/stock_item/{barcode}/{id}', 'Api\StockController@search_item');
Route::post('/api/my_stock_items/check', 'Api\StockController@check_used_item');
Route::get('/api/users/{user_id}', 'Api\UserController@index');

// Order
Route::post('/api/order/process', 'Api\OrderController@process_order');
Route::post('/api/order/accept', 'Api\OrderController@accept_order');
Route::get('/api/order/pending', 'Api\OrderController@get_pending_orders');
Route::get('/api/order/{user_id}', 'Api\OrderController@get_confirm_orders');
Route::get('/api/order/{user_id}/proceeded', 'Api\OrderController@get_proceeded_orders');
Route::get('/api/order/{user_id}/set_check', 'Api\OrderController@set_check_proceeded_orders');
Route::get('/api/order/{user_id}/{order_id}/set_check', 'Api\OrderController@set_check_pended_order');

Route::get('/api/send/{msg}', 'Api\OrderController@send_msg');

Route::get('/seguimiento','FlotillasController@seguimientoGps');
Route::get('/seguir/{id}','FlotillasController@seguir');

Route::resource('marcas', 'MarcasController');

Route::resource('modelos', 'ModelosController');


Route::resource('medidores', 'MedidoresController');
Route::post('medidoresAssign/{id}','MedidoresController@assignTo');
Route::get('changeState/{id}/{state}','MedidoresController@changeState');


Route::post('saveNewGeo','TareasController@newGeo');
Route::post('asignarMedidores','MedidoresController@asignarMultiples');

route::get('imprimirTarea/{id}','TareasController@imprimirTarea');
