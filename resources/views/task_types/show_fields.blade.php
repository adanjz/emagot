<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $taskTypes->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $taskTypes->name !!}</p>
</div>

<!-- Max Time Field -->
<div class="form-group">
    {!! Form::label('max_time', 'Max Time:') !!}
    <p>{!! $taskTypes->max_time !!}</p>
</div>

<!-- Priority Field -->
<div class="form-group">
    {!! Form::label('priority', 'Priority:') !!}
    <p>{!! $taskTypes->priority !!}</p>
</div>

<!-- Workersrequired Field -->
<div class="form-group">
    {!! Form::label('workersRequired', 'Workersrequired:') !!}
    <p>{!! $taskTypes->workersRequired !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $taskTypes->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $taskTypes->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $taskTypes->updated_at !!}</p>
</div>

