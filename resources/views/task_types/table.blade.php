@php($prioridades = ['l'=>'Baja','m'=>'Media','h'=>'Alta','u'=>'Urgente','e'=>'Emergente'])
<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<table class="table table-responsive" id="taskTypes-table">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Alias</th>
            <th>Cliente</th>
            <th>Subtareas</th>
            <th>Tiempo Maximo de Trabajo</th>
            <th>Prioridad</th>
            <th>Operadores requeridos</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($taskTypes as $taskTypes)
        <tr>
            <td>{!! $taskTypes->name !!}</td>
            <td>{!! $taskTypes->alias !!}</td>
            <td>{!! \App\Models\Clientes::find($taskTypes->cliente)->nombre !!}</td>
            <td>{{count($taskTypes->subtasks)}}</td>
            <td>{!! $taskTypes->max_time !!}</td>
            <td>{!! $prioridades[$taskTypes->priority] !!}</td>
            <td>{!! $taskTypes->workersRequired !!}</td>
            <td>
                {!! Form::open(['route' => ['taskTypes.destroy', $taskTypes->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('taskTypes.edit', [$taskTypes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>