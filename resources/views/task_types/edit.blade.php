@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Servicios
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($taskTypes, ['route' => ['taskTypes.update', $taskTypes->id], 'method' => 'patch']) !!}

                        @include('task_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection