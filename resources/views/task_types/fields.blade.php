<style>
    .subtaskRenglon{
        margin:40px 0px;
    }
</style>
<script>
    function addField(){
        fieldId = $('.subtaskRenglon').length;
        $('#subtareas').append('<div class="row subtaskRenglon"><div class="col-md-1">#'+(fieldId+1)+'</div><div class="col-md-11"><div class="camposRow">'+
                '<div class="col-md-12">'+
                    '<label>Nombre</label>' +
                    '<input type="text" class="form-control" name="subtasks['+fieldId+'][nombre]">'+
                '</div>'+
                '<div class="col-md-6 hidden" >' +
                    '<label>Limite de tiempo de ejecución</label>' +
                    '<input type="text" class="form-control timeExecuting" name="subtasks['+fieldId+'][max_time]" value="0">' +
                '</div>' +
                '<div class="col-md-6">' +
                    '<label>Precio</label>' +
                    '<div class="input-group">'+
                    '<span class="input-group-addon">$</span>'+
                    '<input type="text" class="form-control precio" name="subtasks['+fieldId+'][precio]" value="0">' +
                '</div>'+
                '<div class="col-md-6">' +
                    '<label>Alias</label>' +
                    '<div class="input-group">'+
                    '<input type="text" class="form-control alias" name="subtasks['+fieldId+'][alias]" value="0">' +
                    '</div>'+
                '</div>' +
                '<div class="col-md-6 hidden" >' +
                    '<label>Operadores necesarios</label>' +
                    '<input type="number" class="form-control workersRequired" min="1" name="subtasks['+fieldId+'][workersRequired]" value="1">' +
                '</div>' +
            '</div></div></div>');

    }
</script>
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('alias', 'Alias (como viene en el excel):') !!}
    {!! Form::text('alias', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('cliente', 'Cliente:') !!}
    {!! Form::select('cliente', \App\Models\Clientes::get()->pluck('nombre','id')->toArray(), null, ['class' => 'form-control']) !!}
</div>

<!-- Max Time Field -->
<div class="form-group col-sm-6 hidden">
    {!! Form::label('max_time', 'Tiempo máximo de ejecución(horas):') !!}
    {!! Form::number('max_time', 0, ['class' => 'form-control']) !!}
    <label class="subtext" style="color:red">*ingresar 0 para indefinido</label>
</div>
<div class="form-group col-sm-6">

    {!! Form::label('precio', 'Costo Base:') !!}
    <div class="input-group">
        <span class="input-group-addon">$</span>
        {!! Form::text('precio', null, ['class' => 'form-control']) !!}

    </div>
</div>
<!-- Priority Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priority', 'Prioridad:') !!}
    {!! Form::select('priority', ['l'=>'Baja','m'=>'Media','h'=>'Alta','u'=>'Urgente','e'=>'Emergente'],null, ['class' => 'form-control']) !!}
</div>

<!-- Workersrequired Field -->
<div class="form-group col-sm-6 hidden">
    {!! Form::label('workersRequired', 'Operadores necesarios:') !!}
    {!! Form::number('workersRequired', 1, ['class' => 'form-control','min'=>1]) !!}
</div>
<div class="form-group col-md-12">
    <hr>
    {!! Form::label('subtasks', 'Parametros:') !!}
    <br>
    <div class="col-md-12" id="subtareas">
        @if(!empty($taskTypes))
            @foreach($taskTypes->subtasks as $k=>$subtask)
                <input type="hidden" value="{{$subtask->id}}" name="id">
                <div class="row subtaskRenglon" id="row_{{$subtask->id}}">
                    <div class="col-md-1">
                        <a href="javascript:$('#row_{{$subtask->id}}').remove()" class="label label-danger">X</a>#{{$k+1}}</div>
                    <div class="col-md-11">
                        <div class="camposRow">
                            <div class="col-md-12">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="subtasks[{{$k}}][nombre]" value="{{$subtask->name}}">
                            </div>
                            <div class="col-md-6">
                                <label>Precio</label>
                                <input type="text" class="form-control precio" name="subtasks[{{$k}}][precio]" value="{{$subtask->precio}}">
                            </div>
                            <div class="col-md-6">
                                <label>Alias</label>
                                <input type="text" class="form-control alias" name="subtasks[{{$k}}][alias]" value="{{$subtask->alias}}">
                            </div>
                            <div class="col-md-6 hidden">
                                <label>Limite de tiempo de ejecución</label>
                                <input type="text" class="form-control timeExecuting" name="subtasks[{{$k}}][max_time]" value="{{$subtask->max_time}}">
                            </div>
                            <div class="col-md-6 hidden">
                                <label>Operadores necesarios</label>
                                <input type="number" class="form-control workersRequired" min="1" name="subtasks[{{$k}}][workersRequired]" value="{{$subtask->workersRequired}}">
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <a href="javascript:addField()" class="btn btn-default"> + Agregar parametro</a><br><br>

</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('taskTypes.index') !!}" class="btn btn-default">Cancelar</a>
</div>
