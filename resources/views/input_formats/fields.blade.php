<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    #sortable li{
        height: 60px;
        list-style: none;
        margin-bottom: 20px;
    }
    ul{
        padding-left: 0;
    }
    .camposBase{
        border-left: 1px solid;
    }
    #sortable{
        margin-top: 20px;
    }
</style>
<script>
    function addField(){
        fieldId = $('.camposRow').length;
        $('#sortable').append('<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><div class="camposRow">'+
            '<div class="col-md-6">'+
            '<input type="text" name="campo['+fieldId+']">'+
            '</div>'+
            '<div class="col-md-6 camposBase">' +
            '<input type="text" name="key['+fieldId+']">' +
            '</div>' +
            '</div></li>');

    }
    function removeField(e,order){
        $(e).parent().parent().parent().remove();
        $('#deletedFields').val(order+','+$('#deletedFields').val());
    }
    $( function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    } );
</script>
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<!-- Name Field -->
<div class="form-group col-sm-5">
    {!! Form::label('tipo', 'Cliente:') !!}
    {!! Form::select('tipo',\App\Models\Clientes::get()->pluck('nombre','id')->toArray(),(!empty($inputFormats)?$inputFormats->tipo:null), ['class' => 'form-control']) !!}
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <B>Campo Excel</B>
    </div>
    <div class="col-md-6">
        <b>Campo Base</b>
    </div>
    <div class="col-md-12">
        <input type="hidden" id="deletedFields" value="" name="deletedFields">
        <ul id="sortable">
            @if(empty($inputFormats))
                @foreach($campos as $key=>$campo)
                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        <div class="camposRow">
                            <div class="col-md-6">
                                <input type="text" name="campo[{{$key}}]">
                            </div>
                            <div class="col-md-6">
                                {{$campo}}
                                <input type="hidden" name="key[{{$key}}]" value="{{$campo}}">
                            </div>
                        </div>
                    </li>
                @endforeach
            @else
                @foreach(\App\Models\InputFormatRow::where('importFormat',$inputFormats->id)->orderBy('orden')->get() as $key=>$campo)
                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        <div class="camposRow">
                            <div class="col-md-6">
                                <input type="text" name="campo[{{$campo->orden}}]" value="{{$campo->value}}">
                            </div>
                            <div class="col-md-6">
                                @if(in_array($campo->name,$campos))
                                    {{$campo->name}}<input type="hidden" name="key[{{$campo->orden}}]" value="{{$campo->name}}">
                                @else
                                    <input type="text" name="key[{{$campo->orden}}]" value="{{$campo->name}}">
                                    <button class="btn btn-danger" onclick="removeField($(this),{{$campo->id}});">X</button>
                                @endif

                            </div>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
    <a href="javascript:addField()" class="btn btn-default"> + Agregar campo</a><br><br>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('inputFormats.index') !!}" class="btn btn-default">Cancelar</a>
</div>
