@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Input Formats
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($inputFormats, ['route' => ['inputFormats.update', $inputFormats->id], 'method' => 'patch']) !!}

                        @include('input_formats.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection