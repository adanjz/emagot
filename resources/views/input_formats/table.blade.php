<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<table class="table table-responsive" id="inputFormats-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Tipo</th>
            <th >Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($inputFormats as $inputFormats)
        <tr>
            <td>{!! $inputFormats->name !!}</td>
            <td>{!! \App\Models\Clientes::find($inputFormats->tipo)->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['inputFormats.destroy', $inputFormats->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('inputFormats.edit', [$inputFormats->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>