<!-- Legajo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('legajo', 'Legajo:') !!}
    {!! Form::text('legajo', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::Submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('legajos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
