<table class="table table-responsive" id="legajos-table">
    <thead>
        <tr>
            <th>Legajo</th>
            <th>Nombre</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($legajos as $legajos)
        <tr>
            <td>{!! $legajos->legajo !!}</td>
            <td>{!! $legajos->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['legajos.destroy', $legajos->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('legajos.edit', [$legajos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>