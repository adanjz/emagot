<!-- Desde Field -->
<div class="form-group col-sm-6">
    {!! Form::label('desde', 'Desde:') !!}
    {!! Form::date('desde', empty($flotillas)?'':$flotillas->desde, ['class' => 'form-control']) !!}
</div>

<!-- Hasta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hasta', 'Hasta:') !!}
    {!! Form::date('hasta', empty($flotillas)?'':$flotillas->hasta, ['class' => 'form-control']) !!}
</div>

<!-- Legajos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('usuarios', 'Usuarios:') !!}

    {!! Form::select('usuarios[]',\App\Models\Users::where('type',1)->get()->pluck('name','id')->toArray(), $selectedUsuarios, ['class' => 'form-control','multiple'=>'multiple']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('flotillas.index') !!}" class="btn btn-default">Cancel</a>
</div>
