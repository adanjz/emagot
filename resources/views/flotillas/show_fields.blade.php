<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $flotillas->id !!}</p>
</div>

<!-- Desde Field -->
<div class="form-group">
    {!! Form::label('desde', 'Desde:') !!}
    <p>{!! $flotillas->desde !!}</p>
</div>

<!-- Hasta Field -->
<div class="form-group">
    {!! Form::label('hasta', 'Hasta:') !!}
    <p>{!! $flotillas->hasta !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $flotillas->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $flotillas->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $flotillas->updated_at !!}</p>
</div>

