@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Flotillas
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($flotillas, ['route' => ['flotillas.update', $flotillas->id], 'method' => 'patch']) !!}

                        @include('flotillas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection