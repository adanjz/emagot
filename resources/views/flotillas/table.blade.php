<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<table class="table table-responsive" id="flotillas-table">
    <thead>
        <tr>
            <th>Desde</th>
            <th>Hasta</th>
            <th>Miembros</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($flotillas as $flotillas)
        <tr>
            <td>{!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$flotillas->desde)->format('d/m/Y') !!}</td>
            <td>{!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$flotillas->hasta)->format('d/m/Y') !!}</td>
            <td>
                @php($sep = '')
                @foreach($flotillas->flotillasUsuarios as $fu)
                    {{$sep}} {{$fu->user->name}}
                    @php($sep = ',')
                @endforeach
            </td>
            <td>
                {!! Form::open(['route' => ['flotillas.destroy', $flotillas->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('flotillas.edit', [$flotillas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>