@extends('layouts.app')

@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <?php
    $fechaTo = \Carbon\Carbon::now();

    ?>
    <script type="application/javascript">
        $(function () {
            $("#datepicker input").datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy',
                todayHighlight: true,
                onSelect: function(dateText, inst) {
                    alert(dateText);
                }
            }).datepicker('update', new Date('{{$fechaTo->format('Y')}}','{{$fechaTo->format('m')-1}}','{{$fechaTo->format('d')}}'));

        });
    </script>
    <section class="content-header">
        <h1>
            Ruta
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    <form action= '{{url("saveRuta")}}' method='post' files= true enctype="multipart/form-data">
                        <!-- Legajo Field -->
                    {{ csrf_field() }}
                    <!-- Ruta Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('fecha', 'Fecha:') !!}
                            <div id="datepicker" class="input-group date" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" name="fecha" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                            {{--                            {!! Form::date('fecha', null, ['class' => 'form-control']) !!}--}}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::label('tipo', 'Archivos:') !!}
                            {!! Form::select('tipo', \App\Models\InputFormats::get()->pluck('name','id')->toArray(),null, ['class' => 'form-control']) !!}
                        </div>
                        <!-- Ruta Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('ruta', 'Ruta:') !!}
                            {!! Form::file('ruta', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
