<style>
    /* Set the size of the div element that contains the map */
    #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
    }
</style>

<script>
    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var uluru = {lat: {{$tareas->task_latitude}}, lng: {{$tareas->task_longitude}}};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 15, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXzgMgeX0DrtYECOtubJls3laXBFTvtMQ&callback=initMap">
</script>
<!-- Id Field -->
<div class="form-group col-md-12">
    <div id="map"></div>
</div>
<div class="form-group col-md-6">
    {!! Form::label('task_op', 'Task OP:') !!}
    <p>{!! $tareas->task_op !!}</p>
</div>

<div class="form-group col-md-6">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{!! $tareas->task_title !!}</p>
</div>

<!-- Name Field -->
<div class="form-group col-md-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $tareas->task_description !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group col-md-6">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $tareas->task_street !!}</p>
</div>

<!-- Altura Field -->
<div class="form-group col-md-6">
    {!! Form::label('altura', 'Altura:') !!}
    <p>{!! $tareas->task_street_number !!}</p>
</div>

<!-- Trabajo Field -->
<div class="form-group col-md-6">
    {!! Form::label('trabajo', 'Trabajo:') !!}
    <p>{!! $tareas->trabajo !!}</p>
</div>

<!-- Tipo Field -->
<div class="form-group col-md-6">
    {!! Form::label('ot', 'OT:') !!}
    <p>{!! $tareas->task_ot !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group col-md-6">
    {!! Form::label('piso', 'Piso:') !!}
    <p>{!! $tareas->task_floor !!}</p>
</div>
<!-- Descripcion Field -->
<div class="form-group col-md-6">
    {!! Form::label('depto', 'Dpto:') !!}
    <p>{!! $tareas->task_apt !!}</p>
</div>
<!-- Descripcion Field -->
<div class="form-group col-md-6">
    {!! Form::label('op', 'Operación:') !!}
    {{--@php(dd($tareas->task_type))--}}

    <p>@php($taskType = \App\Models\TaskTypes::find($tareas->task_type))
    @if(!empty($taskType))
        {{$taskType->alias}} - {{$taskType->name }}
    @endif
    </p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('dg', 'Dist gest:') !!}
    <p>{!! $tareas->dist_gest !!}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('lote', 'Lote:') !!}
    <p>{!! $tareas->task_observations !!}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('expediente', 'Expediente:') !!}
    <p>{!! $tareas->expediente !!}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $tareas->serie !!}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('marca', 'Marca:') !!}
    <p>{!! $tareas->marca !!}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('task_sub_type', 'Subtipo:') !!}
    @php($subtipo = \App\Models\SubTasks::where('alias',$tareas->task_sub_type)->first())
    <p>{!! $tareas->task_sub_type !!} - {{$subtipo->name}}</p>
</div>
<div class="form-group col-md-12">
    {!! Form::label('task_sub_type', 'Fotos:') !!}
    @foreach($tareas->task_photos as $foto)
        <div class="col-md-4">
            <img style="max-height: 200px;" src="{{$foto->photo_url}}">
        </div>
    @endforeach
</div>

