@extends('layouts.app')

@section('content')
<div>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <title>Editar rutas</title>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXzgMgeX0DrtYECOtubJls3laXBFTvtMQ"></script>
        <style>
            #right-panel {
                font-family: 'Roboto','sans-serif';
                line-height: 30px;
                padding-left: 10px;
            }

            #right-panel select, #right-panel input {
                font-size: 15px;
            }

            #right-panel select {
                width: 100%;
            }

            #right-panel i {
                font-size: 12px;
            }
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map {
                height: 100%;
                width: 100%;
                position: absolute;
                left: 0;
                top:0;
            }
            #right-panel {
                margin: 20px;
                border-width: 2px;
                width: 20%;
                height: 400px;
                float: left;
                text-align: left;
                padding-top: 0;
            }
            #directions-panel {
                margin-top: 10px;
                background-color: #FFEE77;
                padding: 10px;
                overflow: scroll;
                height: 174px;
            }
            .rutas{
                position: absolute;
                background-color: #000;
                color:#fff;
                padding: 10px;
                overflow-y:scroll;
                height: 600px;
            }
            .filtro{
                position: absolute;
                background-color: #000;
                color:#fff;
                padding: 10px;
                overflow-y:scroll;
                height: 600px;
                width: 150px;
                right:0
            }
            .divRuta ol{

                border:1px dashed yellow;
                padding: 0;
                min-height: 40px;
            }
            .divRuta ol li{
                display:none;
                border: 1px solid #fff;
                list-style: none;
                margin-left: 0;
                padding:10px;
            }
            li.selected {
                background-color:GoldenRod
            }
            body.dragging, body.dragging * {
                cursor: move !important;
            }
            select{
                color:#000 !important;
            }
            .dragged {
                position: absolute;
                opacity: 0.5;
                z-index: 2000;
            }

            ol li {
                position: relative;
                /** More li styles **/
            }
            ol li:before {
                position: absolute;
                /** Define arrowhead **/
            }
        </style>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="http://www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>

    </head>
    <div>
        <div id="map"></div>
        <div class="rutas">
            <div id="rutasDiv">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Rutas</h3>
                    </div>
                </div>
                <?php
                function random_color_part() {
                    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
                }

                function random_color() {
                    return random_color_part() . random_color_part() . random_color_part();
                }

                $rutasColores = [];
                $motoActual = 0;
                $lastMoto = 0;
                $sep = '';
                $selector = '';

//ROUTE 0;

                if(empty($rutasColores[0])){
                    $rutasColores[0] = '#'.str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
                    $selector .= $sep.'#ruta_0 ol';
                    $sep = ',';
                }
                ?>

            <i class="fa fa-arrow-down" onclick="showAddresses(0)"></i><label> Pendientes de rutear <select id="nombreMoto_0" onchange="cambiarNombreMoto(0)" class="form-input">
                    <option value="">Pendientes</option>
                </select><div class="colorRuta" style="float: right;margin-left: 10px;margin-top: 4px;background-color:{{$rutasColores[0]}};width: 10px; height: 10px;"></div></label>
                <div class="divRuta" id="ruta_0" color="{{$rutasColores[0]}}">
                    <ol class="divRutaUl">

                        <?php
                        $tasks = \App\Models\Task::whereNull('user_id')->get();
                        ?>
                        @foreach($tasks as $entrev)
                            @if(!empty($entrev->id) && empty(\App\Models\Rutas::where('task_id',$entrev->id)->first()))
                            <li rel="{{$entrev->id}}" class="li" id="{{$entrev->id}}">
                                {{$entrev->task_ot}}-{{$entrev->task_title}}<br>{{$entrev->task_street}} {{$entrev->task_street_number}} {{$entrev->task_floor}} {{$entrev->task_apt}}, {{$entrev->task_localidad}}</li>
                            @endif
                        @endforeach
                    </ol>
                </div>
                @foreach($rutas as $ruta)
                    <?php
                    if(empty($rutasColores[$ruta->ruta_id])){
                        $rutasColores[$ruta->ruta_id] = '#'.str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
                        $selector .= $sep.'#ruta_'.$ruta->ruta_id.' ol';
                        $sep = ',';
                    }
                ?>
                @if($ruta->ruta_id != $motoActual)
                        @if($motoActual != '')
                            </ol>
                        </div>
                        @endif
                        <i class="fa fa-arrow-down" onclick="showAddresses({{$ruta->ruta_id}})"></i>
                        <label> Obrador {{$ruta->ruta_id}}
                            <select id="nombreMoto_{{$ruta->ruta_id}}" onchange="cambiarNombreMoto({{$ruta->ruta_id}})" class="form-input" readonly="readonly">
                                <option value="{{$ruta->user_id}}">{{\App\Models\Flotillas::find($ruta->user_id)->getNombre()}}</option>
                            </select>
                            <div class="colorRuta" style="float: right;margin-left: 10px;margin-top: 4px;background-color:{{$rutasColores[$ruta->ruta_id]}};width: 10px; height: 10px;"></div>
                        </label>
                        <div class="divRuta" id="ruta_{{$ruta->ruta_id}}" color="{{$rutasColores[$ruta->ruta_id]}}">
                            <ol class="divRutaUl">
                                @php($motoActual = $ruta->ruta_id)
                @endif
                @if(!empty($ruta->task_id))
                    @php($entrev = \App\Models\Task::find($ruta->task_id))
                    @if(!empty($entrev->id))
                        <li rel="{{$entrev->id}}" class="li" id="{{$entrev->id}}">
                            {{$entrev->task_ot}}-{{$entrev->task_title}}<br>{{$entrev->task_street}} {{$entrev->task_street_number}} {{$entrev->task_floor}} {{$entrev->task_apt}}, {{$entrev->task_localidad}}
                        </li>
                    @endif
                    @php($lastMoto = $motoActual)
                @endif

        @endforeach
                        </div>
            <a href="/flotillas" class="btn btn-primary addObrador" style="color:#fff">Nuevo operador</a>
        </div>
    </div>
    <script>
        var marker = [];
        var lastMoto = {{$lastMoto}};
        var selector = '{{$selector}}';

        $(document).ready(function(){
            initMap();
            var adjustment;

            $( selector ).sortable({
                connectWith: "ol",
                helper: 'clone',
                update : function () {
                    var order = [];
                    $.each($('.divRuta ol li'), function( index, value ){
                        var newOrder = {};
                        newOrder.id = $(value).parent().parent().attr('id');
                        newOrder.ruta = $(value).attr('id');
                        order.push(newOrder);

                    });
                    $.ajax({
                        type: "POST",
                        method : "POST",
                        url: "{{url('/saveRoutes/rutas')}}",
                        data: {dia:'{{$day}}',order:order,_token:"{{csrf_token()}}"},
                        dataType: "json",
                        success: function (data) {
                        }
                    });
                }
            }).disableSelection();
            $('li').mouseover(function(){
                if($(this).attr('rel') != '' && marker[$(this).attr('rel')] != undefined)
                    marker[$(this).attr('rel')].setIcon(pinSymbol('#000'));
            });
            $('li').mouseout(function(){
                if($(this).attr('rel') != '' && marker[$(this).attr('rel')] != undefined)
                    marker[$(this).attr('rel')].setIcon(pinSymbol($(this).parent().parent().attr('color')));
            });
            $('.checkFranja').click(function(){
                if(!$(this).prop( "checked" )){
                    $.each($('.li_'+$(this).attr('rel')), function( index, value ){
                        if($(this).attr('rel') != '' && marker[$(this).attr('rel')] != undefined)
                            marker[$(this).attr('rel')].setIcon(pinSymbol('#000'));
                    });
                }else{
                    $.each($('.li_'+$(this).attr('rel')), function( index, value ){
                        if($(this).attr('rel') != '' && marker[$(this).attr('rel')] != undefined)
                            marker[$(this).attr('rel')].setIcon(pinSymbol($(this).parent().parent().attr('color')));
                    });
                }
            });
        });
        function repintar(){
            $.each($('.checkFranja'),function(index,value){
                if(!$(this).prop( "checked" )){
                    $.each($('.li_'+$(this).attr('rel')), function( index, value ){
                        if($(this).attr('rel') != '' && marker[$(this).attr('rel')] != undefined)
                            marker[$(this).attr('rel')].setIcon(pinSymbol('#000'));
                    });
                }else{
                    $.each($('.li_'+$(this).attr('rel')), function( index, value ){
                        if($(this).attr('rel') != '' && marker[$(this).attr('rel')] != undefined)
                            marker[$(this).attr('rel')].setIcon(pinSymbol($(this).parent().parent().attr('color')));
                    });
                }
            });
        }
        function createMoto(){
            lastMoto++;
            newColor = getRandomColor();
            $('<i class="fa fa-arrow-down" onclick="showAddresses('+lastMoto+')"></i><label> Obrador '+lastMoto+' <div class="colorRuta" style="float: right;margin-left: 10px;margin-top: 4px;background-color:'+newColor+';width: 10px; height: 10px;"></div></label>' +
                '<div class="divRuta" id="ruta_'+lastMoto+'" color="'+newColor+'" ><ol style="display:block"></ol></div>').insertBefore('.addObrador');
            selector = selector+',#ruta_'+lastMoto+' ol';
            console.log(selector);
            $( selector ).sortable({
                connectWith: "ol",
                helper: 'clone',
                update : function () {
                    var order = [];
                    $.each($('.divRuta ol li'), function( index, value ){
                        var newOrder = {};
                        newOrder.id = $(value).parent().parent().attr('id');
                        newOrder.ruta = $(value).attr('id');
                        order.push(newOrder);

                    });

                        $.ajax({
                            type: "POST",
                            method : "POST",
                            url: "{{url('/saveRoutes/rutas')}}",
                            data: {dia:'<?=$day?>',order:order,_token:"{{csrf_token()}}"},
                            dataType: "json",
                            success: function (data) {
                            }
                        });


                }
            }).disableSelection();
        }
        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function showAddresses(moto){
            $('#ruta_'+moto+' ol li').toggle();
        }
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: {lat:  -34.5092787, lng: -58.5337791}
            });
            var infowindow = new google.maps.InfoWindow();

            @foreach($entrevistas as $k=>$entrevista)
                @if(!empty($entrevista->task_latitude) && !empty($entrevista->task_latitude))
                    <?php
                        $ruta = \App\Models\Rutas::where('task_id',$entrevista->id)->first();
                        if(empty($ruta)){
                            $color = $rutasColores[0];
                        }else{
                            $color = $rutasColores[$ruta->ruta_id];
                        }
                    ?>
                console.log("{{$entrevista->id}} | {{$entrevista->task_latitude}}, {{$entrevista->task_longitude}}");
            marker[{{$entrevista->id}}] = new google.maps.Marker({
                position: new google.maps.LatLng({{$entrevista->task_latitude}}, {{$entrevista->task_longitude}}),
                map: map,
                title:'{{$entrevista->task_title}}<br>{{$entrevista->task_street}} {{$entrevista->task_street_number}}, {{$entrevista->task_localidad}}',
                icon: pinSymbol('{{$color}}')
            });
            google.maps.event.addListener(marker[{{$entrevista->id}}] , 'click', (function(marker , i) {
                return function() {
                    infowindow.setContent('{{$entrevista->task_title}}<br>{{$entrevista->task_street}} {{$entrevista->task_street_number}}, {{$entrevista->task_localidad}}');
                    infowindow.open(map, marker );
                }
            })(marker[{{$entrevista->id}}] ,{{$k}}));
            @endif
            @endforeach
        }
        function pinSymbol(color) {
            return {
                path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
                fillColor: color,
                fillOpacity: 1,
                strokeColor: '#000',
                strokeWeight: 2,
                scale: 1,
            };
        }
    </script>
@endsection
