@extends('layouts.app')

@section('content')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    @if(empty($fechaDesde))
        @php($fechaDesde = \Carbon\Carbon::now()->subMonth()->format('d/m/Y'))
        @php($fechaHasta = \Carbon\Carbon::now()->format('d/m/Y'))

    @endif
    <script>
        $(function () {
            $("#datepicker input").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                todayHighlight: true,
                clearBtn: true,
                onSelect: function(dateText, inst) {
                    alert(dateText);
                }
            }).datepicker('update', new Date('{{empty($fechaTo)?\Carbon\Carbon::now()->format('Y'):$fechaTo->format('Y')}}','{{empty($fechaTo)?\Carbon\Carbon::now()->format('m'):$fechaTo->format('m')-1}}','{{empty($fechaTo)?'':$fechaTo->format('d')}}'));
            @if(empty($fechaTo))
                $('#datepicker').datepicker('update','');
            @endif
            $('.clearDate').click(function(){
                $('#datepicker').datepicker('update','');
            });
            $('#periodo').daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                startDate:"{{$fechaDesde}}",
                endDate:"{{$fechaHasta}}"
            }, function(start, end, label) {
                $('#fromDate').val(start.format('YYYY-MM-DD'));
                $('#toDate').val(end.format('YYYY-MM-DD'));
            });
        });
    </script>
    <style>
        .clearDate{
            position: absolute;
            top: 0px;
            margin-top: 33px;
            z-index: 99999;
            color: red;
            margin-left: 95px;
        }
    </style>
    <section class="content-header">
        <h1 class="pull-left">Tareas</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('tareas.create') !!}">Nuevo</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        <div class="box box-primary">
            <div class="box-body">
                <form action="/buscarTareas" method="POST">
                {{ csrf_field() }}
                <!-- Date -->
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Periodo:</label>
                                <input type="text" name="fromd" id="periodo" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- Date and time range -->
                            <div class="form-group">
                                <label>Direccion:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control pull-right" id="direccion" name="direccion" value="{{!empty($direccion)?$direccion:''}}">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                        <div class="col-md-2">
                            <!-- Date and time range -->
                            <div class="form-group">
                                <label>Documento:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control pull-right" id="ruta" name="ruta" value="{{!empty($documento)?$documento:''}}">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                        <input type="hidden" id="fromDate" name="from">
                        <input type="hidden" id="toDate" name="to">
                        <div class="col-md-2">
                            <!-- Date and time range -->
                            <div class="form-group">
                                <label>Clase:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control pull-right" id="poliza" name="poliza" value="{{!empty($clase)?$clase:''}}">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                        <div class="col-md-2">
                            <!-- Date and time range -->
                            <div class="form-group">
                                <label>Cliente:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control pull-right" id="cliente" name="cliente" value="{{!empty($cliente)?$cliente:''}}">
                                </div>
                            </div>
                            <!-- /.form group -->
                        </div>
                        <div class="col-md-2">
                            <!-- Date and time range -->
                            <div class="form-group">
                                <label>Medidor:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control pull-right" id="medidor" name="medidor" value="{{!empty($medidor)?$medidor:''}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <input type="submit" value="Buscar" class="btn btn-primary">
                        </div>
                        <div class="col-md-8">
                            <h5>@if(!empty($ruta))
                                    @php($r = \App\Models\Rutas::find($ruta))
                                    @if(!empty($r))
                                        {{$r->nombre}}
                                    @endif
                                @endif
                            </h5>
                        </div>
                    </div>

                    <!-- /.form group -->


                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('tareas.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

