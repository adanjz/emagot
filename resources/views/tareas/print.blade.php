<html>
<body onload="window.print();">
<table width="100%" border="1">
    <tr>
        <td>
            <b>{!! Form::label('task_op', 'Task OP:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->task_op !!}</p>
        </td>
        <td>
            <b>{!! Form::label('titulo', 'Titulo:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->task_title !!}</p>
        </td>
    </tr>
    <tr>
        <td>
            <b>{!! Form::label('descripcion', 'Descripcion:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->task_description !!}</p>
        </td>
        <td>
            <b>{!! Form::label('calle', 'Calle:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->task_street !!}</p>
        </td>
    </tr>
    <tr>
        <td>
            <b>{!! Form::label('altura', 'Altura:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->task_street_number !!}</p>
        </td>
        <td>
            <b>{!! Form::label('trabajo', 'Trabajo:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->trabajo !!}</p>
        </td>
    </tr>
    <tr>
        <td>
            <b>{!! Form::label('ot', 'OT:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->task_ot !!}</p>
        </td>
        <td>
            <b>{!! Form::label('piso', 'Piso:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->task_floor !!}</p>
        </td>
    </tr>
    <tr>
        <td>
            <b>{!! Form::label('depto', 'Dpto:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->task_apt !!}</p>
        </td>
        <td>
            <b>{!! Form::label('op', 'Operación:') !!}</b>
        </td>
        <td>
            <p>@php($taskType = \App\Models\TaskTypes::find($tareas->task_type))
                @if(!empty($taskType))
                    {{$taskType->alias}} - {{$taskType->name }}
                @endif
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <b>{!! Form::label('dg', 'Dist gest:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->dist_gest !!}</p>
        </td>
        <td>
            <b>{!! Form::label('lote', 'Lote:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->lote !!}</p>
        </td>
    </tr>
    <tr>
        <td>
            <b>{!! Form::label('expediente', 'Expediente:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->expediente !!}</p>
        </td>
        <td>
            <b>{!! Form::label('serie', 'Serie:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->serie !!}</p>
        </td>
    </tr>
    <tr>
        <td>
            <b>{!! Form::label('marca', 'Marca:') !!}</b>
        </td>
        <td>
            <p>{!! $tareas->marca !!}</p>
        </td>
        <td>
            <b>{!! Form::label('task_sub_type', 'Subtipo:') !!}</b>
        </td>
        <td>
            @php($subtipo = \App\Models\SubTasks::where('alias',$tareas->task_sub_type)->first())
            @if(!empty($subtipo))
                <p>{!! $tareas->task_sub_type !!} - {{$subtipo->name}}</p>
            @endif
        </td>
    </tr>
</table>
@foreach($tareas->task_photos as $foto)
        <img style="width:30%;padding:10px;" src="{{$foto->photo_url}}" style="float:left">
@endforeach
</body>
</html>
