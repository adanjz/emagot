@php($status = empty($status)?0:$status)
@php($lotes = empty($lotes)?[]:$lotes)
<style>
    #map {
        height: 400px;
        width: 100%;
        margin: 0px;
        padding: 0px
    }
    .selectedRow{
        background-color: greenyellow;
    }
    #fecha{
        margin-top: 5px;
        border-radius: 5px;
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&ext=.js&key=AIzaSyCXzgMgeX0DrtYECOtubJls3laXBFTvtMQ"></script>
<script>
    @php($rutasColores[0] = '#'.str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT))
    @foreach($rutas as $ruta)
            @php($rutasColores[$ruta->ruta_id] = '#'.str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT).str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT))
    @endforeach
    var poly;
    var polygon;
    var markers=[];
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: {lat:  -34.5092787, lng: -58.5337791}
        });
        var infowindow = new google.maps.InfoWindow();

        @foreach($tareas as $k=>$entrevista)
            @if(!empty($entrevista->task_latitude) && !empty($entrevista->task_latitude))
                    <?php
                        $ruta = \App\Models\Rutas::where('task_id',$entrevista->id)->first();
                        if(empty($ruta)){
                            $color = $rutasColores[0];
                        }else{
                            $color = $rutasColores[$ruta->ruta_id];
                        }
                    ?>

            var marker_{{$k}} = new google.maps.Marker({
                position: new google.maps.LatLng({{$entrevista->task_latitude}}, {{$entrevista->task_longitude}}),
                map: map,
                name: '{{$entrevista->task_title}}',
                item_id:'{{$entrevista->id}}',
                draggable:true,
                title:'{{$entrevista->task_op}} \n {{$entrevista->task_street}} {{$entrevista->task_street_number}}, {{$entrevista->task_localidad}}',
                icon: pinSymbol('{{$color}}')
            });
            google.maps.event.addListener(marker_{{$k}} , 'click', (function(marker_{{$k}}, i) {
                return function() {
                    infowindow.setContent('<b>Task OP:</b> {{$entrevista->task_op}}<br> <b>Dirección:</b> {{$entrevista->task_street}} {{$entrevista->task_street_number}}, {{$entrevista->task_localidad}} <br> <b>Flotilla:</b> {{ $entrevista->getRutaNombre() }}<br> <b>Fecha:</b> {{ $entrevista->fecha }}');
                    infowindow.open(map, marker_{{$k}});
                }
            })(marker_{{$k}} ,{{$k}}));
            markers.push(marker_{{$k}});


            var positionStart;
            google.maps.event.addListener(marker_{{$k}}, 'dragstart', function() {
                positionStart = this.position;
            });

            google.maps.event.addListener(marker_{{$k}}, 'dragend', function()
            {
                if (confirm("¿Estás seguro de que quieres mover este marcador?")) {
                    geocodePosition(marker_{{$k}}.getPosition(),marker_{{$k}}.item_id);

                } else {
                    marker_{{$k}}.setPosition(positionStart);
                }

            });
            @endif
        @endforeach
         polygon = new google.maps.Polygon({
            strokeColor: "#1E41AA",
            strokeOpacity: 1.0,
            strokeWeight: 3,
            map: map,
            fillColor: "#2652F2",
            fillOpacity: 0.6
        });

        poly = polygon.getPath();

        function geocodePosition(pos,item)
        {
            geocoder = new google.maps.Geocoder();
            geocoder.geocode
            ({
                    latLng: pos
                },
                function(results, status)
                {
                    if (status == google.maps.GeocoderStatus.OK)
                    {
                        console.log(results[0]);

                        $.ajax({
                            type: "POST",
                            method : "POST",
                            url: "{{url('/saveNewGeo/')}}",
                            data: {item:item,lat:results[0].geometry.location.lat,long:results[0].geometry.location.lng,_token:"{{csrf_token()}}"},
                            dataType: "json",
                            success: function (data) {
                                alert('Geoposicion cambiada');
                            }
                        });
//                        alert(results[0].formatted_address);
//                        $("#mapSearchInput").val(results[0].formatted_address);
//                        $("#mapErrorMsg").hide(100);
                    }
                    else
                    {
                        alert('No se encuentra geoposicion valida');
//                        $("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
                    }
                }
            );
        }
        function addPolyPoints(e) {
            poly.push(e.latLng);
            var markerCnt = 0;
            $('.filas').removeClass('selectedRow');
            for (var i=0; i<markers.length; i++) {
                if (google.maps.geometry.poly.containsLocation(markers[i].getPosition(), polygon)) {
                    markerCnt++;
                    $('#fila_'+markers[i].item_id).addClass('selectedRow');
                }
            }
            document.getElementById('markersSelected').innerHTML = markerCnt;
        }

        google.maps.event.addListener(map, 'click', addPolyPoints);
        $('#resetPolygon').click(function() {
            polygon.setMap(null);
            polygon = new google.maps.Polygon({
                strokeColor: "#1E41AA",
                strokeOpacity: 1.0,
                strokeWeight: 3,
                map: map,
                fillColor: "#2652F2",
                fillOpacity: 0.6
            });
            poly = polygon.getPath();
            document.getElementById('markersSelected').innerHTML = "0";
            $('.filas').removeClass('selectedRow');
        });


        $('.radiofilter').click(function(){
            $val = $(this).val();
            $lote = $('#filedlote').val();
            console.log($lote);
            location.href = '{{url("/tareas/filtrar")}}'+"/"+$val + "/" + $lote;
        });

        $('#filedlote').change(function(){
            var estatus 
           $.each($('.radiofilter'), function(){
                if($(this).prop('checked') == true){
                    estatus = $(this).val();
                }
            });

            $val = $(this).val();
            location.href = '{{url("/tareas/filtrar")}}'+"/"+ estatus + "/" + $val;
        })
    }

    google.maps.event.addDomListener(window, "load", initMap);
    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 2,
            scale: 1,
        };
    }
    function asignarDirecciones(){
        var flotilla = $('#flotilla').val();
        var fecha = $('#fecha').val();
        var direcciones = [];
        $('.filas.selectedRow').each(function(k,item){
            direcciones.push($(item).attr('ref'));
        });
        if(direcciones.length > 0){
            $.ajax({
                type: "POST",
                method : "POST",
                url: "{{url('/saveRoutesPolygon/rutas')}}",
                data: {flotilla:flotilla,direccion:direcciones,fecha:fecha,_token:"{{csrf_token()}}"},
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }else{
            alert('Seleccione direcciones para realizar esta accion');
        }


    }
</script>
<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<a href="javascript:void(0)" id="resetPolygon" class="btn btn-danger" style="    margin-bottom: 10px;">Borrar poligono</a>

<div id="map"></div>
<div class="row box" style="    margin-left: 0px;    margin-top: 20px;">

        <div class="col-md-3">
            <div id="info"><h4>Direcciones seleccionadas: <span id="markersSelected">0</span></h4></div>
        </div>
    <div class="col-md-1">
        <h4>Fecha:</h4>
    </div>
        <div class="col-md-3">
           <input type="date" id="fecha" name="fecha" class="form-control">
        </div>
        <div class="col-md-3">
            <h4>
            Legajo: <select class="form-cotrol" id="flotilla">
                @foreach($flotillas as $flotilla)
                    <option value="{{$flotilla->id}}">{{$flotilla->getNombre()}}</option>
                @endforeach
            </select>
            </h4>
        </div>
        <div class="col-md-2">
            <h4>
                <a href="javascript:asignarDirecciones()" class="btn btn-xl btn-success">Asignar</a>
            </h4>
        </div>
</div>
<div class="filter_status">
<div class="row">
    <div class="col-md-6">   
        <form>
            <div>
                <h6><strong> Estado </strong></h6>
            </div>
            <div>
                @if($status == 3)
                <input type="radio" class="radiofilter" id="all" name="drone" value="3" checked/>
                @else
                <input type="radio" class="radiofilter" id="all" name="drone" value="3" />
                @endif
                <label for="all">Dif. Cerradas</label> &nbsp;&nbsp;

                @if($status == 0)
                <input type="radio" class="radiofilter" id="notasign" name="drone" value="0" checked/>
                @else
                <input type="radio" class="radiofilter" id="notasign" name="drone" value="0"/>
                @endif
                <label for="notasign">Sin Asignar</label> &nbsp;&nbsp;

                @if($status == 1)                
                <input type="radio" class="radiofilter" id="asign" name="drone" value="1"  checked/>
                @else
                <input type="radio" class="radiofilter" id="asign" name="drone" value="1" />
                @endif
                <label for="asign">Asignadas</label>&nbsp;&nbsp;

                @if($status == 2)                                
                <input type="radio" class="radiofilter" id="closed" name="drone" value="2" checked/>
                @else
                <input type="radio" class="radiofilter" id="closed" name="drone" value="2" />
                @endif
                <label for="closed">Cerradas</label>
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <form action="">
            <div class="form-group">
                <label for="filedlote">Lote</label>
                <select name="filedlote" id="filedlote" class="form-control" value="all">
                    <option value="all" selected>Seleccionar Lote</option>
                    @foreach($lotes as $lote)
                        <option value="{{$lote}}">{{$lote}}</option>                    
                    @endforeach
                </select>
            </div>
        </form>
    </div>
</div>
</div>
<table class="table table-responsive" id="tareas-table">
    <thead>
        <tr>
            <th>#</th>
            <th>Task OP</th>
            <th>Lote</th>
            <th>Servicio</th>
            <th>Parametro</th>
            <th>Flotilla</th>
            <th>Name</th>
            <th>Calle</th>
            <th>Altura</th>
            <th>Descripcion</th>
            <th>Fecha</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tareas as $tareas)
        <?php
        $tareaAlias = '';
        $subtareaAlias = '';
        $servicio = \App\Models\TaskTypes::find($tareas->task_type);
        if(!empty($servicio)){
            $tareaAlias = $servicio->name;
            $subservicio = \App\Models\SubTasks::where('alias',$tareas->task_sub_type)->where('taskType',$tareas->task_type)->first();
            if(!empty($subservicio)){
                $subtareaAlias = $subservicio->name;
            }
        }


        ?>
        <tr id="fila_{{$tareas->id}}" class="filas" ref="{{$tareas->id}}">
            <td>{{$tareas->id}}</td>
            <td>{{$tareas->task_op}}</td>
            <td>{{$tareas->task_observations}}</td>
            <td>{{$tareaAlias}}</td>
            <td>{{$subtareaAlias}}</td>
            <td>{{$tareas->getRutaNombre()}}</td>
            <td>{!! $tareas->task_title !!}</td>
            <td>{!! $tareas->task_street !!}</td>
            <td>{!! $tareas->task_street_number !!}</td>
            <td>{!! $tareas->task_description !!}</td>
            <td>{{$tareas->fecha}}</td>
            <td>
                {!! Form::open(['route' => ['tareas.destroy', $tareas->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tareas.show', [$tareas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    {{--<a href="{!! route('tareas.edit', [$tareas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>--}}
                    <a target="_blank" href="/imprimirTarea/{{$tareas->id}}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-print"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
