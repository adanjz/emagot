@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tareas
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tareas, ['route' => ['tareas.update', $tareas->id], 'method' => 'patch']) !!}

                        @include('tareas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection