@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Medidores</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('medidores.create') !!}">Nuevo</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-body">
                    @include('medidores.table')
            </div>
        </div>
        <div class="box box-success" style="width: 98%">
            <div class="box-body">
                <div class="row" style="padding-left: 20px;margin-right: 0px;">
                    <h3>Asignar seleccionados a flotilla</h3>
                    <div class="form-group col-sm-6">
                        {!! Form::label('legajo_id', 'Legajo:') !!}
                        {!! Form::select('legajo_id', \App\Models\Users::all()->pluck('name','id')->toArray(),null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <button onclick="asignarSeleccionados()" class="btn btn-primary">Asignar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

