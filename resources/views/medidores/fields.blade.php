<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::select('marca', \App\Models\Modelos::get()->pluck('nombre','id')->toArray() ,null, ['class' => 'form-control']) !!}
</div>
@if(empty($medidores))
<div class="form-group col-sm-6">
    {!! Form::label('prefijo', 'Prefijo:') !!}
    {!! Form::text('prefijo', null, ['class' => 'form-control']) !!}
</div>
<!-- Serie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rango', 'Rango:') !!}
    <br>
    <div class="form-group col-sm-6">
        {!! Form::number('rango_d', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::number('rango_h', null, ['class' => 'form-control']) !!}
    </div>
</div>

    @else
    <div class="form-group col-sm-6">
        {!! Form::label('serie', 'Serie:') !!}
        {!! Form::text('serie', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('medidores.index') !!}" class="btn btn-default">Cancelar</a>
</div>
