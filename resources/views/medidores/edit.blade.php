@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Medidores
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($medidores, ['route' => ['medidores.update', $medidores->id], 'method' => 'patch']) !!}

                        @include('medidores.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection