<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )

        $('[data-toggle="tooltip"]').tooltip()
    });
    function asignarSeleccionados(){

        var seleccionados = [];
        $.each($("input[class='medidores_select']:checked"), function(){
            seleccionados.push($(this).val());
        });
        $.ajax({
            type: "POST",
            method : "POST",
            url: "{{url('/asignarMedidores/')}}",
            data: {medidores:seleccionados,legajo:$('#legajo_id').val(),_token:"{{csrf_token()}}"},
            dataType: "json",
            success: function (data) {
                alert('Geoposicion cambiada');
            }
        });
    }
</script>
<table class="table table-responsive" id="medidores-table">
    <thead>
        <tr>
            <th>S</th>
            <th>Serie</th>
            <th>Modelo</th>
            <th>Estado Actual</th>
            <th>Ultima Lectura</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($medidores as $medidores)
        @php($estados = [0=>'',1=>'Nuevo',2=>'Asignado',3=>'Instalado',4=>'Retirado'])
        @php($estado = \App\Models\MedidorLogs::where('medidor',$medidores->id)->orderBy('id','desc')->first())
        <tr>
            <td>
                @if($estado->estado != 3 && $estado->estado != 4)
                    <input type="checkbox" class="medidores_select" value="{{$medidores->id}}">
                @endif
            </td>
            <td>{{ $medidores->serie }}</td>
            @php($modelo = \App\Models\Modelos::find($medidores->modelo))
            <td>{{ empty($modelo)?'':$modelo->nombre }}</td>

            <td>{{$estados[$estado->estado]}}


            </td>
            <td>{{$medidores->lectura}}</td>
            <td>
                {!! Form::open(['route' => ['medidores.destroy', $medidores->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    @if($estado->estado == 4)
                        <a href="/changeState/{{$medidores->id}}/{{1}}" class='btn btn-success btn-xs' title="Cambiar a nuevo" data-toggle="tooltip" data-placement="top"><i class="glyphicon glyphicon-wrench"></i></a>
                    @endif
                    <a href="{!! route('medidores.show', [$medidores->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('medidores.edit', [$medidores->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
