<div class="box box-warning" style="width: 98%">
    <div class="box-body">
        <div class="row" style="padding-left: 20px;margin-right: 0px;">

        @php($modelo = \App\Models\Modelos::find($medidores->modelo))
        <h3>Medidor: {{$modelo->nombre}}{{$medidores->serie}}</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>Estado</th>
                    <th>Descripcion</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
            @php($estados = ['','Nuevo','En flotilla','Instalado','Retirado','Devuelto al deposito','Enviado al cliente'])
            @php($firstState = '')
                @foreach(\App\Models\MedidorLogs::where('medidor',$medidores->id)->orderBy('created_at','desc')->get() as $logs)
                    @if(empty($firstState))
                        @php($firstState = $logs->estado)
                    @endif
                    <tr>
                        <td>{{$estados[$logs->estado]}}</td>
                        <td>{{$logs->descripcion}}</td>
                        <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$logs->created_at)->format('d/m/Y H:i:s')}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@switch($firstState)
@case (1)
<div class="box box-success" style="width: 98%">
    <div class="box-body">
        <div class="row" style="padding-left: 20px;margin-right: 0px;">
            <h3>Asignar a flotilla</h3>
            <form action="/medidoresAssign/{{$medidores->id}}" method="post">
                {{csrf_field()}}
                <div class="form-group col-sm-6">
                    {!! Form::label('legajo_id', 'Legajo:') !!}
                    {!! Form::select('legajo_id', \App\Models\Users::all()->pluck('name','id')->toArray(),null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::Submit('Guardar', ['class' => 'btn btn-primary']) !!}
                </div>
            </form>

        </div>
    </div>
</div>
@break
@endswitch