<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<table class="table table-responsive" id="items-table">
    <thead>
        <tr>
            <th>Tipo</th>
            <th>Código de barras</th>
            <th>Nombre</th>
            <th>Número serie</th>
            <th>Foto</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
    @foreach($items as $items)
        <tr>
            @php($tipo = \App\Models\MaterialTypes::find($items->codigo))
            <td>{{ empty($tipo)?'':$tipo->name }}</td>
            <td>{!! $items->item_barcode !!}</td>
            <td>{!! $items->item_name !!}</td>
            <td>{!! $items->numero_serie !!}</td>
            <td><img src="{{Storage::url('items/'.$items->item_image) }}" width="100" height="100"></td>
            <td>
                {!! Form::open(['route' => ['items.destroy', $items->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('items.edit', [$items->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
