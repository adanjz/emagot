<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo', 'Tipo:') !!}
    {!! Form::select('codigo', \App\Models\MaterialTypes::get()->pluck('name','id')->toArray() ,null, ['class' => 'form-control']) !!}
</div>

<!-- Codigo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item_barcode', 'Código de barras:') !!}
    {!! Form::text('item_barcode', null, ['class' => 'form-control']) !!}
</div>
<!-- Nombre Field -->
<div class="form-group col-sm-6 material-nombre">
    {!! Form::label('item_name', 'Nombre:') !!}
    {!! Form::text('item_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Numero serie -->
<div class="form-group col-sm-6 material-nombre">
    {!! Form::label('numero_serie', 'Número serie:') !!}
    {!! Form::text('numero_serie', null, ['class' => 'form-control']) !!}
</div>

<!-- Foto Field -->
<div class="form-group col-sm-6 material-foto">
    {!! Form::label('item_image', 'Foto:') !!}
    {!! Form::file('item_image', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::Submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('items.index') !!}" class="btn btn-default">Cancelar</a>
</div>
