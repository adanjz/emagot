<!-- Flotilla Field -->
<div class="form-group col-sm-6">
    {!! Form::label('flotilla', 'Flotilla:') !!}
    {!! Form::number('flotilla', null, ['class' => 'form-control']) !!}
</div>

<!-- Usuario Field -->
<div class="form-group col-sm-6">
    {!! Form::label('usuario', 'Usuario:') !!}
    {!! Form::number('usuario', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('flotillasUsuarios.index') !!}" class="btn btn-default">Cancel</a>
</div>
