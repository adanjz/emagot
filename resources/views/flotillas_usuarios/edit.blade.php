@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Flotillas Usuarios
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($flotillasUsuarios, ['route' => ['flotillasUsuarios.update', $flotillasUsuarios->id], 'method' => 'patch']) !!}

                        @include('flotillas_usuarios.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection