<table class="table table-responsive" id="flotillasUsuarios-table">
    <thead>
        <tr>
            <th>Flotilla</th>
        <th>Usuario</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($flotillasUsuarios as $flotillasUsuarios)
        <tr>
            <td>{!! $flotillasUsuarios->flotilla !!}</td>
            <td>{!! $flotillasUsuarios->usuario !!}</td>
            <td>
                {!! Form::open(['route' => ['flotillasUsuarios.destroy', $flotillasUsuarios->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('flotillasUsuarios.show', [$flotillasUsuarios->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('flotillasUsuarios.edit', [$flotillasUsuarios->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>