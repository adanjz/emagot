<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $flotillasUsuarios->id !!}</p>
</div>

<!-- Flotilla Field -->
<div class="form-group">
    {!! Form::label('flotilla', 'Flotilla:') !!}
    <p>{!! $flotillasUsuarios->flotilla !!}</p>
</div>

<!-- Usuario Field -->
<div class="form-group">
    {!! Form::label('usuario', 'Usuario:') !!}
    <p>{!! $flotillasUsuarios->usuario !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $flotillasUsuarios->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $flotillasUsuarios->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $flotillasUsuarios->updated_at !!}</p>
</div>

