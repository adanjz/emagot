<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::select('marca', \App\Models\Marcas::get()->pluck('nombre','id')->toArray() ,null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alias', 'Alias:') !!}
    {!! Form::text('alias', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('tipo', 'Tipo:') !!}
    <select name="tipo" class="form-control">
        <option value="0" {{(!empty($medidores) && $medidores->tipo == 0)?'selected':'' }}>Gas</option>
        <option value="1" {{(!empty($medidores) && $medidores->tipo == 1)?'selected':'' }}>Agua</option>
        <option value="2" {{(!empty($medidores) && $medidores->tipo == 2)?'selected':'' }}>Electricidad</option>
    </select>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('capacidad', 'Capacidad (M3/h | Volts):') !!}
    {!! Form::number('capacidad', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('modelos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
