<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<table class="table table-responsive" id="modelos-table">
    <thead>
        <tr>
            <th>Marca</th>
            <th>Nombre</th>
            <th>Alias</th>
            <th>Tipo</th>
            <th>Capacidad</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($modelos as $modelos)
        <tr>
            @php($marca = \App\Models\Marcas::find($modelos->marca))
            <td>{{ empty($marca)?'':$marca->nombre }}</td>
            <td>{!! $modelos->nombre !!}</td>
            <td>{!! $modelos->alias !!}</td>
            <td>{!! $modelos->tipo !!}</td>
            <td>{!! $modelos->capacidad !!}</td>
            <td>
                {!! Form::open(['route' => ['modelos.destroy', $modelos->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('modelos.edit', [$modelos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
