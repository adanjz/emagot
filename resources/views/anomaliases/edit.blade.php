@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Anomalias
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($anomalias, ['route' => ['anomaliases.update', $anomalias->id], 'method' => 'patch']) !!}

                        @include('anomaliases.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection