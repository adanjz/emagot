<!-- Anomalia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anomalia', 'Anomalia:') !!}
    {!! Form::text('anomalia', null, ['class' => 'form-control','required'=>true]) !!}
</div>

<!-- Codigo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo', 'Codigo:') !!}
    {!! Form::text('codigo', null, ['class' => 'form-control','required'=>true]) !!}
</div>
<!-- Codigo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo_cliente', 'Codigo interno:') !!}
    {!! Form::text('codigo_cliente', null, ['class' => 'form-control','required'=>true]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::Submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('anomaliases.index') !!}" class="btn btn-default">Cancelar</a>
</div>
