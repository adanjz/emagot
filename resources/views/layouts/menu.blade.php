<li class="treeview {{(Request::is('tareas*')||Request::is('modificarRutas') || Request::is('buscarTareas'))?'active':''}}">
    <a href="#"><i class="fa fa-tasks"></i><span>Tareas</span>
        <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu {{(Request::is('tareas*')|| Request::is('buscarTareas'))?'menu-open':''}}">
        <li class="{{ Request::is('tareas*')|| Request::is('buscarTareas') ? 'active' : '' }}">
            <a href="/tareas"><i class="fa fa-list"></i><span>Listado de tareas</span></a>
        </li>
        <li class="{{ Request::is('tareas*') ? 'active' : '' }}">
            <a href="/importarTareas"><i class="fa fa-map-marked-alt"></i><span>Nueva ruta</span></a>
        </li>
        <li class="{{ Request::is('tareas*')||Request::is('modificarRutas') ? 'active' : '' }}">
            <a href="/modificarRutas"><i class="fa fa-map-marker"></i><span>Modificar ruta</span></a>
        </li>
    </ul>
</li>

<li class="treeview {{(Request::is('marcas*') || Request::is('modelos*') || Request::is('medidores*') )?'active':''}}">
    <a href="#"><i class="fa fa-box-open"></i><span>Conf. Medidores</span>
        <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu {{(Request::is('marcas*') || Request::is('modelos*') || Request::is('medidores*'))?'menu-open':''}}">
        <li class="{{ Request::is('marcas*') ? 'active' : '' }}">
            <a href="{!! route('marcas.index') !!}"><i class="fa fa-edit"></i><span>Marcas</span></a>
        </li>

        <li class="{{ Request::is('modelos*') ? 'active' : '' }}">
            <a href="{!! route('modelos.index') !!}"><i class="fa fa-edit"></i><span>Modelos</span></a>
        </li>

        <li class="{{ Request::is('medidores*') ? 'active' : '' }}">
            <a href="{!! route('medidores.index') !!}"><i class="fa fa-tachometer-alt"></i><span>Medidores</span></a>
        </li>
    </ul>
</li>

{{--<li class="{{ Request::is('tareas*') ? 'active' : '' }}">--}}
        {{--<a href="{!! route('tareas.index') !!}"><i class="fa fa-barcode"></i><span>Tareas</span></a>--}}
{{--</li>--}}
<li class="treeview {{(Request::is('stocks*') || Request::is('items*') )?'active':''}}">
    <a href="#"><i class="fa fa-box-open"></i><span>Sistema de stock</span>
        <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu {{(Request::is('stocks*') || Request::is('items*'))?'menu-open':''}}">
        <li class="{{ Request::is('items*') ? 'active' : '' }}">
            <a href="{!! route('items.index') !!}"><i class="fa fa-wrench"></i><span>Productos</span></a>
        </li>
        <li class="{{ Request::is('stocks*') ? 'active' : '' }}">
            <a href="{!! route('stocks.index') !!}"><i class="fa fa-barcode"></i><span>Stock</span></a>
        </li>
    </ul>
</li>
<li class="treeview {{(Request::is('flotillas*')||Request::is('anomaliases*') || Request::is('taskTypes*')|| Request::is('legajos*')|| Request::is('users*') || Request::is('clientes*') || Request::is('inputFormats*') || Request::is('materialTypes*'))?'active':''}}">
    <a href="#"><i class="fa fa-cogs"></i><span>Configuraciones</span>
        <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu {{(Request::is('flotillas*')|| Request::is('anomaliases*')||Request::is('taskTypes*') || Request::is('legajos*') || Request::is('users*') || Request::is('clientes*') || Request::is('inputFormats*') || Request::is('materialTypes*'))?'menu-open':''}}">
        <li class="{{ Request::is('anomaliases*') ? 'active' : '' }}">
            <a href="{!! route('anomaliases.index') !!}"><i class="fa fa-edit"></i><span>Codigos de rendicion</span></a>
        </li>
        <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
            <a href="{!! route('clientes.index') !!}"><i class="fa fa-building"></i><span>Clientes</span></a>
        </li>
        <li class="{{ Request::is('users*') ? 'active' : '' }}">
            <a href="{!! route('users.index') !!}"><i class="fa fa-users"></i><span>Usuarios</span></a>
        </li>
        {{--<li class="{{ Request::is('legajos*') ? 'active' : '' }}">--}}
            {{--<a href="{!! route('legajos.index') !!}"><i class="fa fa-user"></i><span>Legajos</span></a>--}}
        {{--</li>--}}
        <li class="{{ Request::is('inputFormats*') ? 'active' : '' }}">
            <a href="{!! route('inputFormats.index') !!}"><i class="fa fa-upload"></i><span>Archivos customizados</span></a>
        </li>

        <li class="{{ Request::is('taskTypes*') ? 'active' : '' }}">
            <a href="{!! route('taskTypes.index') !!}"><i class="fa fa-briefcase"></i><span>Tipos de Servicio</span></a>
        </li>

        {{--<li class="{{ Request::is('subTasks*') ? 'active' : '' }}">--}}
            {{--<a href="{!! route('subTasks.index') !!}"><i class="fa fa-edit"></i><span>Sub Tasks</span></a>--}}
        {{--</li>--}}

        <li class="{{ Request::is('materialTypes*') ? 'active' : '' }}">
            <a href="{!! route('materialTypes.index') !!}"><i class="fa fa-wrench"></i><span>Tipos de material</span></a>
        </li>

        <li class="{{ Request::is('flotillas*') ? 'active' : '' }}">
            <a href="{!! route('flotillas.index') !!}"><i class="fa fa-truck"></i><span>Flotillas</span></a>
        </li>

        {{--<li class="{{ Request::is('flotillasUsuarios*') ? 'active' : '' }}">--}}
            {{--<a href="{!! route('flotillasUsuarios.index') !!}"><i class="fa fa-edit"></i><span>Flotillas Usuarios</span></a>--}}
        {{--</li>--}}
    </ul>
</li>
<li class="{{ Request::is('legajo_gps*') ? 'active' : '' }}">
    <a href="/seguimiento"><i class="fa fa-map"></i><span>Seguimiento</span></a>
</li>

