<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<table class="table table-responsive" id="marcas-table">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Alias</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($marcas as $marcas)
        <tr>
            <td>{!! $marcas->nombre !!}</td>
            <td>{!! $marcas->alias !!}</td>
            <td>
                {!! Form::open(['route' => ['marcas.destroy', $marcas->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('marcas.edit', [$marcas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>