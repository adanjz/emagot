<table class="table table-responsive" id="subTasks-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Max Time</th>
        <th>Priority</th>
        <th>Workersrequired</th>
        <th>Tasktype</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($subTasks as $subTasks)
        <tr>
            <td>{!! $subTasks->name !!}</td>
            <td>{!! $subTasks->max_time !!}</td>
            <td>{!! $subTasks->priority !!}</td>
            <td>{!! $subTasks->workersRequired !!}</td>
            <td>{!! $subTasks->taskType !!}</td>
            <td>
                {!! Form::open(['route' => ['subTasks.destroy', $subTasks->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('subTasks.show', [$subTasks->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('subTasks.edit', [$subTasks->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>