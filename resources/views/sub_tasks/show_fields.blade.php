<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $subTasks->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $subTasks->name !!}</p>
</div>

<!-- Max Time Field -->
<div class="form-group">
    {!! Form::label('max_time', 'Max Time:') !!}
    <p>{!! $subTasks->max_time !!}</p>
</div>

<!-- Priority Field -->
<div class="form-group">
    {!! Form::label('priority', 'Priority:') !!}
    <p>{!! $subTasks->priority !!}</p>
</div>

<!-- Workersrequired Field -->
<div class="form-group">
    {!! Form::label('workersRequired', 'Workersrequired:') !!}
    <p>{!! $subTasks->workersRequired !!}</p>
</div>

<!-- Tasktype Field -->
<div class="form-group">
    {!! Form::label('taskType', 'Tasktype:') !!}
    <p>{!! $subTasks->taskType !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $subTasks->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $subTasks->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $subTasks->updated_at !!}</p>
</div>

