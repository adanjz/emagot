<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_time', 'Max Time:') !!}
    {!! Form::number('max_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Priority Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priority', 'Priority:') !!}
    {!! Form::text('priority', null, ['class' => 'form-control']) !!}
</div>

<!-- Workersrequired Field -->
<div class="form-group col-sm-6">
    {!! Form::label('workersRequired', 'Workersrequired:') !!}
    {!! Form::number('workersRequired', null, ['class' => 'form-control']) !!}
</div>

<!-- Tasktype Field -->
<div class="form-group col-sm-6">
    {!! Form::label('taskType', 'Tasktype:') !!}
    {!! Form::number('taskType', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('subTasks.index') !!}" class="btn btn-default">Cancel</a>
</div>
