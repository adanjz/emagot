@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Legajo Gps
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($legajoGps, ['route' => ['legajoGps.update', $legajoGps->id], 'method' => 'patch']) !!}

                        @include('legajo_gps.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection