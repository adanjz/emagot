<table class="table table-responsive" id="legajoGps-table">
    <thead>
        <tr>
            <th>Nombre</th>
            <th colspan="3">Seguir</th>
        </tr>
    </thead>
    <tbody>
    @foreach($legajoGps as $legajoGps)
        <tr>
            <td>{!! $legajoGps->name !!}</td>
            <td>
                <a href="/seguir/{{$legajoGps->id}}"><i class="fa fa-map-marker"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>