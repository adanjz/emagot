<!-- Legajo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('legajo_id', 'Legajo Id:') !!}
    {!! Form::number('legajo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::Submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('legajoGps.index') !!}" class="btn btn-default">Cancelar</a>
</div>
