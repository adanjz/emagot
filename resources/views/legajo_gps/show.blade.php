@extends('layouts.app')

@section('content')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXzgMgeX0DrtYECOtubJls3laXBFTvtMQ"></script>
<style>
    #map {
        height: 500px;
        width: 100%;
        position: absolute;
        left: 0;
        top:0;
    }
</style>
    <script>
        @if(!empty($gps))
        $(document).ready(function(){
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: {lat:  -34.5092787, lng: -58.5337791}
            });
            marker = new google.maps.Marker({
                position: new google.maps.LatLng({{$gps->latitude}}, {{$gps->longitude}}),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: {
                    url: '/person.png',
                }

                });
        });
        function pinSymbol(color) {
            return {
                path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
                fillColor: color,
                fillOpacity: 1,
                strokeColor: '#000',
                strokeWeight: 2,
                scale: 1,
            };
        }
        @endif
    </script>
    <section class="content-header">
        <h1>
            Seguimiento: {{$legajo->name}}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <div id="map"></div>
                    <a href="{!! route('legajoGps.index') !!}" class="btn btn-default">Atras</a>
                </div>
            </div>
        </div>
    </div>
@endsection
