<!-- Item Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item_id', 'Item:') !!}
    {!! Form::select('item_id', \App\Models\StockItem::all()->pluck('item_name','id')->toArray(),null, ['class' => 'form-control']) !!}
</div>

{{--@php($flotillas = \App\Models\Flotillas::whereDate('desde','<=',\Carbon\Carbon::now()->format('Y-m-d'))->whereDate('hasta','>=',\Carbon\Carbon::now()->format('Y-m-d'))->get())--}}
<!-- Legajo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'Usuario:') !!}
    {{--<select id="legajo_id" name="legajo_id" class="form-control">--}}
        {{--@foreach($flotillas as $flotilla)--}}
            {{--<option value="{{$flotilla->id}}">{{$flotilla->getNombre()}}</option>--}}
        {{--@endforeach--}}
    {{--</select>--}}
    {!! Form::select('user_id', \App\Models\Users::all()->pluck('name','id')->toArray(),null, ['class' => 'form-control']) !!}
</div>


<!-- Cantidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo', 'Tipo:') !!}
    {!! Form::select('tipo',['1'=>'+','2'=>'-'], null, ['class' => 'form-control']) !!}
</div>

<!-- Cantidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item_amount', 'Cantidad:') !!}
    {!! Form::number('item_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::Submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('stocks.index') !!}" class="btn btn-default">Cancelar</a>
</div>
