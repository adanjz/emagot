<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<table class="table table-responsive" id="stocks-table">
    <thead>
        <tr>
            <th>Item</th>
        <th>Legajo</th>
            <th>Tipo</th>
        <th>Cantidad</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($stocks as $stock)
        <tr>
            @php($item = \App\Models\StockItem::find($stock->item_id))
            <td>{{ empty($item)?'':$item->item_name }}</td>
            @php($flotilla = \App\Models\Users::find($stock->user_id))
            <td>{{ empty($flotilla)?'':$flotilla->name }}</td>
            <td>+</td>
            <td>{!! $stock->item_amount !!}</td>
            <td>
                {!! Form::open(['route' => ['stocks.destroy', $stock->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
{{--                    <a href="{!! route('stocks.edit', [$stock->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>--}}
{{--                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}--}}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>