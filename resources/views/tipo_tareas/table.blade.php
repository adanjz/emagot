<table class="table table-responsive" id="tipoTareas-table">
    <thead>
        <tr>
            <th>Nombre</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tipoTareas as $tipoTarea)
        <tr>
            <td>{!! $tipoTarea->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['tipoTareas.destroy', $tipoTarea->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tipoTareas.show', [$tipoTarea->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tipoTareas.edit', [$tipoTarea->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>