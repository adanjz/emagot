<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<!-- Max Uses Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_uses', 'Usos máximos:') !!}
    {!! Form::number('max_uses', null, ['class' => 'form-control']) !!}
</div>
<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::select('marca',array_merge(['0' => 'Seleccione'], \App\Models\Marcas::get()->pluck('nombre','id')->toArray() ) ,null, ['class' => 'form-control']) !!}
</div>
<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::select('modelo',array_merge(['0' => 'Seleccione'], \App\Models\Modelos::get()->pluck('nombre','id')->toArray())  ,null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('materialTypes.index') !!}" class="btn btn-default">Cancelar</a>
</div>
