<script>
    $(document).ready( function () {
        var table = $('.table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        } )
    });
</script>
<table class="table table-responsive" id="materialTypes-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Max Uses</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
    @foreach($materialTypes as $materialTypes)
        <tr>
            @php($marca = \App\Models\Marcas::find($materialTypes->marca))
            @php($modelo = \App\Models\Modelos::find($materialTypes->modelo))
            <td>{!! $materialTypes->name !!}</td>
            <td>{{ empty($marca)?'':$marca->nombre }}</td>
            <td>{{ empty($modelo)?'':$modelo->nombre }}</td>
            <td>{!! $materialTypes->max_uses !!}</td>
            <td>
                {!! Form::open(['route' => ['materialTypes.destroy', $materialTypes->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('materialTypes.edit', [$materialTypes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
