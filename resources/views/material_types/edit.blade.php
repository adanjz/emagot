@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tipo de Materiales
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($materialTypes, ['route' => ['materialTypes.update', $materialTypes->id], 'method' => 'patch']) !!}

                        @include('material_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection