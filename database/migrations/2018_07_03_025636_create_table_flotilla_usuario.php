<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFlotillaUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flotillas_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flotilla')->unsigned();
            $table->integer('usuario')->unsigned();
            $table->foreign('flotilla')->references('id')->on('flotillas');
            $table->foreign('usuario')->references('id')->on('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
