<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('sender_id')->nullable(); //user id whose sent order
            $table->foreign('sender_id')->references('id')->on('users');

            $table->unsignedInteger('receiver_id')->nullable(); //user id whose to accept order
            $table->foreign('receiver_id')->references('id')->on('users');

            $table->unsignedInteger('item_id')->nullable(); //order item id
            $table->foreign('item_id')->references('id')->on('stock_items');

            $table->integer('order_type')->default(0); //order type, 0: send order, 1: request order

            $table->unsignedInteger('item_amount');
            $table->unsignedInteger('item_checked_amount')->default(0);
            $table->integer('order_result')->default(0); // contract result,0: not proceeded, 1: proceeded.

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('process_orders');
    }
}
