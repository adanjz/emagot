<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subTasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('max_time',10,2)->nullable();
            $table->string('priority')->nullable();
            $table->integer('workersRequired')->nullable();
            $table->integer('taskType')->unsigned();
            $table->foreign('taskType')->references('id')->on('taskTypes');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
