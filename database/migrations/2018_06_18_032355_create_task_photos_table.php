<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('task_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo_url', 255)->nullable();
            $table->unsignedInteger('task_id')->nullable(); // user id who are doing this task
            $table->foreign('task_id')->references('id')->on('tasks');
            $table->tinyInteger('photo_type'); //0:start task photo, 1:pending task photo, 2:finished photo
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_photos');
    }
}
