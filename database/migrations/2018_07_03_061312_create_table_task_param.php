<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTaskParam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_params', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task')->unsigned();
            $table->string('columna');
            $table->text('valor')->nullable();
            $table->integer('direc')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('task')->references('id')->on('tasks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
