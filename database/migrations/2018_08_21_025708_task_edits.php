<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaskEdits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('task_op')->nullable();
            $table->string('dist_gest')->nullable();
            $table->string('lote')->nullable();
            $table->string('fecha_generación')->nullable();
            $table->string('expediente')->nullable();
            $table->string('fecha_ingreso')->nullable();
            $table->string('ruta')->nullable();
            $table->string('serie')->nullable();
            $table->string('marca')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
