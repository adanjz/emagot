<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task_title')->nullable();
            $table->unsignedInteger('user_id')->nullable(); // user id who are doing this task
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('task_description')->nullable();
            $table->string('task_street');
            $table->string('task_street_number');
            $table->string('task_ot');
            $table->string('task_floor')->nullable();
            $table->string('task_apt')->nullable();
            $table->string('task_type')->default("");
            $table->integer('task_urgency')->default(0);//0:not urgent, 1:not so urgent, 2:urgent
            $table->string('task_latitude')->nullable();
            $table->string('task_longitude')->nullable();
            $table->integer('task_state')->default(0); //0:open, 1: pending, 2:complete
            $table->string('task_photo')->nullable();
            $table->string('task_started_at')->nullable();
            $table->string('task_finished_at')->nullable();
            $table->string('task_localidad')->nullable();
            $table->date('fecha')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
