<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User();
        $user->name='adan';
        $user->email='adanzweig@gmail.com';
        $user->face_id = '';
        $user->password = \Illuminate\Support\Facades\Hash::make('123456');
        $user->save();
    }
}
