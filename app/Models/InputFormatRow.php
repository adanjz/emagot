<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InputFormatRow extends Model
{
    public $table = 'importFormatsFields';
    public static function translateColumn($campo){
        switch ($campo){
            case 'servicio':
                $v = 'task_type';
                break;
            case 'parametro_servicio':
                $v = 'task_sub_type';
                break;
            default:
                $v = $campo;
        }
        return $v;
    }
}
