<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedidorLogs extends Model
{
    protected $table = 'medidor_logs';
}
