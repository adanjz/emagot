<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FlotillasUsuarios
 * @package App\Models
 * @version July 3, 2018, 3:03 am UTC
 *
 * @property \App\Models\Flotilla flotilla
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection rutas
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property integer usuario
 */
class FlotillasUsuarios extends Model
{
    use SoftDeletes;

    public $table = 'flotillas_usuarios';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'flotilla',
        'usuario'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'flotilla' => 'integer',
        'usuario' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function flotilla()
    {
        return $this->belongsTo(\App\Models\Flotilla::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class,'usuario');
    }
}
