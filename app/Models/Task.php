<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
        'id',
        'task_street',
        'task_street_number',
        'task_floor',
        'tsk_apt',
        'task_type',
        'task_urgency',
        'task_ot',
        'task_description',
        'task_title',
        'task_observations'
    ];
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function anomalies() {
        return $this->hasMany('App\Models\TaskAnomaly');
    }
    public function task_photos() {
        return $this->hasMany('App\Models\TaskPhoto', 'task_id', 'id');
    }
    public static function translateColumn($var){
        $keys = [   
                    'titulo'        =>'task_title',
                    'descripcion'   =>'task_description',
                    'calle'         =>'task_street',
                    'servicio'      =>'task_type',
                    'parametro_servicio' =>'task_sub_type',
                    'altura'        =>'task_street_number',
                    'nro_puerta'    =>'task_street_number',
                    
                    'piso'          =>'task_floor',
                    'dpto'          =>'task_apt',
                    'localidad'     =>'task_localidad',
                    'servicio'         =>'taskType',
                    'nro_lote'      =>'lote',
                    'observaciones_de_la_ods' => 'task_observations',
                    'task_observations' => 'task_observations'
            ];
        return empty($keys[$var])?$var:$keys[$var];
    }
    public function getFechaRuta(){
        $rutas = Rutas::where('task_id',$this->id)->first();
        if(!empty($rutas)){
            return $rutas->fecha;
        }else{
            return "Pendiente";
        }

    }
    public function getRutaNombre(){
        $rutas = Rutas::where('task_id',$this->id)->first();
        if(empty($rutas)){
            return "Pendiente";
        }else{
            $rutaRoot = Rutas::whereNotNull('user_id')->where('ruta_id',$rutas->ruta_id)->first();
            if(empty($rutaRoot)){
                return "Pendiente";
            }else{
                $flotilla = Flotillas::find($rutaRoot->user_id);
                if(empty($flotilla)){
                    return "Pendiente";
                }else{
                    return $flotilla->getNombre();
                }
            }

        }

        return "Pendiente";
    }
}
