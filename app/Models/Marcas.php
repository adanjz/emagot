<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Marcas
 * @package App\Models
 * @version February 20, 2019, 3:51 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection flotillasUsuarios
 * @property \Illuminate\Database\Eloquent\Collection proceededOrders
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property string nombre
 */
class Marcas extends Model
{
//    use SoftDeletes;

    public $table = 'marcas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre','alias'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'alias' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
