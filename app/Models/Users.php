<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Users
 * @package App\Models
 * @version June 1, 2018, 2:38 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property string name
 * @property string email
 * @property string password
 * @property string remember_token
 */
class Users extends Model
{

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'password',
        'remember_token',
        'face_id',
        'fcm_token'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function tasks() {
        return $this->hasMany('App\Models\Task');
    }

    public function sigin_state() {
        return $this->hasMany('App\Models\LogedinUser');
    }
    
}
