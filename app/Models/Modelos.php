<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Modelos
 * @package App\Models
 * @version February 20, 2019, 3:51 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection flotillasUsuarios
 * @property \Illuminate\Database\Eloquent\Collection proceededOrders
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property integer marca
 * @property string nombre
 */
class Modelos extends Model
{
//    use SoftDeletes;

    public $table = 'modelos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'marca',
        'nombre',
        'alias',
        'capacidad',
        'tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'marca' => 'integer',
        'nombre' => 'string',
        'alias' => 'string',
        'capacidad' => 'integer',
        'tipo' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
