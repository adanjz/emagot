<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Legajos
 * @package App\Models
 * @version June 1, 2018, 2:23 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection LegajoGp
 * @property \Illuminate\Database\Eloquent\Collection Stock
 * @property string legajo
 * @property string nombre
 */
class Legajos extends Model
{
    use SoftDeletes;

    public $table = 'legajo';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'legajo',
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'legajo' => 'string',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function legajoGps()
    {
        return $this->hasMany(\App\Models\LegajoGp::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function stocks()
    {
        return $this->hasMany(\App\Models\Stock::class);
    }
}
