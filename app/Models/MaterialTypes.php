<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MaterialTypes
 * @package App\Models
 * @version July 3, 2018, 3:02 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection flotillasUsuarios
 * @property \Illuminate\Database\Eloquent\Collection rutas
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property string name
 * @property integer max_uses
 * @property integer marca
 * @property integer modelo
 */
class MaterialTypes extends Model
{
    use SoftDeletes;

    public $table = 'materialTypes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'max_uses',
        'marca',
        'modelo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'max_uses' => 'integer',
        'marca' => 'integer',
        'modelo' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
