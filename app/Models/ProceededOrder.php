<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProceededOrder extends Model
{
    protected $table = 'proceeded_orders';
    protected $fillable = ['order_id', 'check_result', 'user_id'];

    public function order() {
        return $this->hasOne('App\Models\ProcessOrder', 'id', 'order_id');
    }
}
