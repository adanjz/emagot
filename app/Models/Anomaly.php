<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Anomaly extends Model
{
    public function TaskAnomaly() {
        return $this->belongsTo('App\Models\TaskAnomaly');
    }
}
