<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tareas
 * @package App\Models
 * @version June 1, 2018, 2:24 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property string name
 * @property string calle
 * @property string altura
 * @property string trabajo
 * @property string tipo
 * @property string descripcion
 */
class Tareas extends Model
{
    use SoftDeletes;

    public $table = 'tareas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'calle',
        'altura',
        'trabajo',
        'tipo',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'calle' => 'string',
        'altura' => 'string',
        'trabajo' => 'string',
        'tipo' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
