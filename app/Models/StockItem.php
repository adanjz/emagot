<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockItem extends Model
{
    protected $fillable = [
        'item_name',
        'item_barcode',
        'item_image',
        'codigo',
        'numero_serie'

    ];

    public function stock() {
        return $this->belongsToMany('App\Models\Stock');
    }
    public function order() {
        return $this->belongsToMany('App\Models\ProcessOrder');
    }
}
