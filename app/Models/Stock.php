<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'user_id', 'item_id', 'item_amount'
    ];

    public function stock_item() {
        return $this->hasOne('App\Models\StockItem', 'id', 'item_id');
    }
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
