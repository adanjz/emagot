<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Anomalias
 * @package App\Models
 * @version June 1, 2018, 2:25 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property string anomalia
 * @property string codigo
 */
class Anomalias extends Model
{
    use SoftDeletes;

    public $table = 'anomalias';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'anomalia',
        'codigo',
        'codigo_cliente'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'anomalia' => 'string',
        'codigo' => 'string',
        'codigo_cliente' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    public function TaskAnomaly() {
        return $this->belongsTo('App\Models\TaskAnomaly','anomaly_id','id');
    }
    
}
