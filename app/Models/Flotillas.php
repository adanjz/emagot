<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Flotillas
 * @package App\Models
 * @version July 3, 2018, 3:03 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection FlotillasUsuario
 * @property \Illuminate\Database\Eloquent\Collection rutas
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property date desde
 * @property date hasta
 */
class Flotillas extends Model
{
    use SoftDeletes;

    public $table = 'flotillas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'desde',
        'hasta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'desde' => 'date',
        'hasta' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function flotillasUsuarios()
    {
        return $this->hasMany(\App\Models\FlotillasUsuarios::class,'flotilla');
    }
    public function getNombre()
    {
        $sep = '';
        $res = '';
        foreach ($this->flotillasUsuarios as $fu){
            $res .= $sep . $fu->user->name;
            $sep = ',';
        }
        return $res;
    }
}
