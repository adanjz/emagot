<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskAnomaly extends Model
{
    protected $fillable = [
        'user_id', 'task_id', 'anomaly_id'
    ];
    public function task() {
        return $this->belongsTo('App\Models\Task');
    }
    public function anomaly() {
        return $this->hasOne('App\Models\Anomaly', 'id', 'anomaly_id');
    }
}
