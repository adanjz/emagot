<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
    protected $table = "user_locations";
    protected $fillable = ['user_id', 'latitude', 'longitude'];

    public function user() {
        return $this->hasOne('App\User');
    }
}
