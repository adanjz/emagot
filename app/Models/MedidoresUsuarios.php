<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedidoresUsuarios extends Model
{
    protected $table = 'medidores_usuarios';
}
