<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";
    protected $fillable = [
        'name', 'face_id',
    ];

    public function tasks() {
        return $this->hasMany('Task');
    }

    public function sigin_state() {
        return $this->hasMany('LogedinUser');
    }
}
