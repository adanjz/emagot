<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LegajoGps
 * @package App\Models
 * @version June 1, 2018, 2:24 am UTC
 *
 * @property \App\Models\Legajo legajo
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property integer legajo_id
 * @property string latitude
 * @property string longitude
 */
class LegajoGps extends Model
{
    use SoftDeletes;

    public $table = 'legajo_gps';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'legajo_id',
        'latitude',
        'longitude'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'legajo_id' => 'integer',
        'latitude' => 'string',
        'longitude' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function legajo()
    {
        return $this->belongsTo(\App\Models\Legajo::class);
    }
}
