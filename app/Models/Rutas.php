<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rutas extends Model
{
    public $table = 'rutas';

    public $fillable = [
        'fecha',
        'user_id',
        'order',
        'ruta_id',
        'task_id'
    ];

}
