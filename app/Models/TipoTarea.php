<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TipoTarea
 * @package App\Models
 * @version June 1, 2018, 2:24 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property string nombre
 */
class TipoTarea extends Model
{
    use SoftDeletes;

    public $table = 'tipo_tarea';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
