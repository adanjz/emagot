<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessOrder extends Model
{
    protected $table = "process_orders";
    protected $fillable = ["sender_id", "receiver_id", "item_id", "order_type", "item_amount", "item_checked_amount"];

    public function user() {
        return $this->hasOne('App\User', 'id', 'receiver_id');
    }
    public function item() {
        return $this->hasOne('App\Models\StockItem', 'id', 'item_id');
    }
    public function proceeded_order() {
        return $this->belongsTo('App\Models\ProceededOrder');
    }
}
