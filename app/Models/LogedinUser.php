<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogedinUser extends Model
{
    protected $fillable = ['user_id', 'imei', 'sign_state'];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
