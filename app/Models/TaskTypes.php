<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TaskTypes
 * @package App\Models
 * @version July 3, 2018, 3:02 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection flotillasUsuarios
 * @property \Illuminate\Database\Eloquent\Collection rutas
 * @property \Illuminate\Database\Eloquent\Collection stock
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property \Illuminate\Database\Eloquent\Collection SubTask
 * @property string name
 * @property float max_time
 * @property string priority
 * @property integer workersRequired
 */
class TaskTypes extends Model
{
    use SoftDeletes;

    public $table = 'taskTypes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'max_time',
        'priority',
        'workersRequired'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'max_time' => 'float',
        'priority' => 'string',
        'workersRequired' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subTasks()
    {
        return $this->hasMany(\App\Models\SubTasks::class,'taskType');
    }
}
