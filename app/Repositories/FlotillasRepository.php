<?php

namespace App\Repositories;

use App\Models\Flotillas;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FlotillasRepository
 * @package App\Repositories
 * @version July 3, 2018, 3:03 am UTC
 *
 * @method Flotillas findWithoutFail($id, $columns = ['*'])
 * @method Flotillas find($id, $columns = ['*'])
 * @method Flotillas first($columns = ['*'])
*/
class FlotillasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'desde',
        'hasta'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Flotillas::class;
    }
}
