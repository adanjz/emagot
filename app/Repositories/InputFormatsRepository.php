<?php

namespace App\Repositories;

use App\Models\InputFormats;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InputFormatsRepository
 * @package App\Repositories
 * @version July 3, 2018, 3:01 am UTC
 *
 * @method InputFormats findWithoutFail($id, $columns = ['*'])
 * @method InputFormats find($id, $columns = ['*'])
 * @method InputFormats first($columns = ['*'])
*/
class InputFormatsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'tipo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InputFormats::class;
    }
}
