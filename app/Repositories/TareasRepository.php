<?php

namespace App\Repositories;

use App\Models\Tareas;
use App\Models\Task;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TareasRepository
 * @package App\Repositories
 * @version June 1, 2018, 2:24 am UTC
 *
 * @method Tareas findWithoutFail($id, $columns = ['*'])
 * @method Tareas find($id, $columns = ['*'])
 * @method Tareas first($columns = ['*'])
*/
class TareasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'task_name',
        'calle',
        'altura',
        'trabajo',
        'tipo',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Task::class;
    }
}
