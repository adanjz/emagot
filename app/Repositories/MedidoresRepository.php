<?php

namespace App\Repositories;

use App\Models\Medidores;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MedidoresRepository
 * @package App\Repositories
 * @version February 20, 2019, 3:51 am UTC
 *
 * @method Medidores findWithoutFail($id, $columns = ['*'])
 * @method Medidores find($id, $columns = ['*'])
 * @method Medidores first($columns = ['*'])
*/
class MedidoresRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'serie',
        'modelo',
        'tipo',
        'capacidad'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Medidores::class;
    }
}
