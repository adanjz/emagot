<?php

namespace App\Repositories;

use App\Models\MaterialTypes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MaterialTypesRepository
 * @package App\Repositories
 * @version July 3, 2018, 3:02 am UTC
 *
 * @method MaterialTypes findWithoutFail($id, $columns = ['*'])
 * @method MaterialTypes find($id, $columns = ['*'])
 * @method MaterialTypes first($columns = ['*'])
*/
class MaterialTypesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'max_uses',
        'marca',
        'modelo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MaterialTypes::class;
    }
}
