<?php

namespace App\Repositories;

use App\Models\Modelos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ModelosRepository
 * @package App\Repositories
 * @version February 20, 2019, 3:51 am UTC
 *
 * @method Modelos findWithoutFail($id, $columns = ['*'])
 * @method Modelos find($id, $columns = ['*'])
 * @method Modelos first($columns = ['*'])
*/
class ModelosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'marca',
        'nombre',
        'alias'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Modelos::class;
    }
}
