<?php

namespace App\Repositories;

use App\Models\Legajos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LegajosRepository
 * @package App\Repositories
 * @version June 1, 2018, 2:23 am UTC
 *
 * @method Legajos findWithoutFail($id, $columns = ['*'])
 * @method Legajos find($id, $columns = ['*'])
 * @method Legajos first($columns = ['*'])
*/
class LegajosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'legajo',
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Legajos::class;
    }
}
