<?php

namespace App\Repositories;

use App\Models\Items;
use App\Models\StockItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ItemsRepository
 * @package App\Repositories
 * @version June 1, 2018, 2:23 am UTC
 *
 * @method Items findWithoutFail($id, $columns = ['*'])
 * @method Items find($id, $columns = ['*'])
 * @method Items first($columns = ['*'])
*/
class ItemsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_name',
        'item_image',
        'tipo',
        'codigo',
        'item_barcode'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StockItem::class;
    }
}
