<?php

namespace App\Repositories;

use App\Models\TipoTarea;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TipoTareaRepository
 * @package App\Repositories
 * @version June 1, 2018, 2:24 am UTC
 *
 * @method TipoTarea findWithoutFail($id, $columns = ['*'])
 * @method TipoTarea find($id, $columns = ['*'])
 * @method TipoTarea first($columns = ['*'])
*/
class TipoTareaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoTarea::class;
    }
}
