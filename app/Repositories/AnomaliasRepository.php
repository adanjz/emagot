<?php

namespace App\Repositories;

use App\Models\Anomalias;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AnomaliasRepository
 * @package App\Repositories
 * @version June 1, 2018, 2:25 am UTC
 *
 * @method Anomalias findWithoutFail($id, $columns = ['*'])
 * @method Anomalias find($id, $columns = ['*'])
 * @method Anomalias first($columns = ['*'])
*/
class AnomaliasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'anomalia',
        'codigo',
        'codigo_cliente'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Anomalias::class;
    }
}
