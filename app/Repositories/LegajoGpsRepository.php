<?php

namespace App\Repositories;

use App\Models\LegajoGps;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LegajoGpsRepository
 * @package App\Repositories
 * @version June 1, 2018, 2:24 am UTC
 *
 * @method LegajoGps findWithoutFail($id, $columns = ['*'])
 * @method LegajoGps find($id, $columns = ['*'])
 * @method LegajoGps first($columns = ['*'])
*/
class LegajoGpsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'legajo_id',
        'latitude',
        'longitude'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LegajoGps::class;
    }
}
