<?php

namespace App\Repositories;

use App\Models\FlotillasUsuarios;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FlotillasUsuariosRepository
 * @package App\Repositories
 * @version July 3, 2018, 3:03 am UTC
 *
 * @method FlotillasUsuarios findWithoutFail($id, $columns = ['*'])
 * @method FlotillasUsuarios find($id, $columns = ['*'])
 * @method FlotillasUsuarios first($columns = ['*'])
*/
class FlotillasUsuariosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'flotilla',
        'usuario'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FlotillasUsuarios::class;
    }
}
