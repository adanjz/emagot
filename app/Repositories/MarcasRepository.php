<?php

namespace App\Repositories;

use App\Models\Marcas;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MarcasRepository
 * @package App\Repositories
 * @version February 20, 2019, 3:51 am UTC
 *
 * @method Marcas findWithoutFail($id, $columns = ['*'])
 * @method Marcas find($id, $columns = ['*'])
 * @method Marcas first($columns = ['*'])
*/
class MarcasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre','alias'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Marcas::class;
    }
}
