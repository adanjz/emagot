<?php

namespace App\Repositories;

use App\Models\TaskTypes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TaskTypesRepository
 * @package App\Repositories
 * @version July 3, 2018, 3:02 am UTC
 *
 * @method TaskTypes findWithoutFail($id, $columns = ['*'])
 * @method TaskTypes find($id, $columns = ['*'])
 * @method TaskTypes first($columns = ['*'])
*/
class TaskTypesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'max_time',
        'priority',
        'workersRequired'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TaskTypes::class;
    }
}
