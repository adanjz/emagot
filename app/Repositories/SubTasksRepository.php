<?php

namespace App\Repositories;

use App\Models\SubTasks;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SubTasksRepository
 * @package App\Repositories
 * @version July 3, 2018, 3:02 am UTC
 *
 * @method SubTasks findWithoutFail($id, $columns = ['*'])
 * @method SubTasks find($id, $columns = ['*'])
 * @method SubTasks first($columns = ['*'])
*/
class SubTasksRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'max_time',
        'priority',
        'workersRequired',
        'taskType'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubTasks::class;
    }
}
