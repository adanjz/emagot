<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/1/2018
 * Time: 3:46 AM
 */

namespace App\Http;


class Service
{
    public static function make_resp($status, $code, $message) {
        return compact('status', 'code', 'message');
    }
    public static function validate_data($data) {
        return $data && count($data) > 0;
    }
}