<?php

namespace App\Http\Controllers\Api;

use App\Http\Service;
use App\Models\Anomalias;
use App\Models\Anomaly;
use App\Models\Flotillas;
use App\Models\FlotillasUsuarios;
use App\Models\Rutas;
use App\Models\Task;
use App\Models\TaskAnomaly;
use App\Models\TaskPhoto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the current user's task.
     * @param $id
     * @return array
     */
    public function index($id)
    {
        $flotillas = array_flatten(FlotillasUsuarios::where('usuario', $id)->get()->pluck('flotilla')->toArray());

        $rutas = array_flatten(Rutas::where('fecha',Carbon::now()->format('Y-m-d'))
            ->whereIn('user_id',$flotillas)
            ->get()
            ->pluck('task_id')
            ->toArray());


        $tasks = Task::whereIn('id', $rutas)
            ->orderBy('updated_at', 'asc')->get();


//        $tasks = Task::where('user_id', $id)
//            ->orderBy('updated_at', 'asc')->get();

        if(Service::validate_data($tasks)) {
            $response = Service::make_resp( "success" , 100, "all tasks");
            $response['tasks'] = $tasks;
            return $response;
        }

        $response = Service::make_resp( "fail" , 203, "No tasks");
        $response['tasks'] = [];
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the detail of specified task.
     *
     * @param  int  $id
     * @return array
     */
    public function show($id)
    {
        $task = Task::find($id);
        $task_photos = TaskPhoto::where('task_id', $id)->get();
        $task_anomalies = $task->anomalies;//get checked anomaly from TaskAnomaly task
        foreach ($task_anomalies as $task_anomaly) {
            $task_anomaly->anomaly;
        }

        $anomaly_list = Anomalias::select('id', 'anomalia', 'codigo')->get();// all anomalies
//        $anomaly_list = Anomaly::select('id', 'anomaly_name', 'code')->get();// all anomalies
        if(!$anomaly_list) $anomaly_list = [];
        if($task) {
            $response = Service::make_resp( "success" , 100, "task");
            $response['task'] = $task;
            $response['countIn'] = rand(0,3);
            $response['countOut'] = rand(0,3);
            $response['task_photos'] = $task_photos;
            $response['anomaly_list'] = $anomaly_list;
            return $response;
        }
        $response = Service::make_resp( "fail" , 200, "no task");
        $response['task'] = [];
        $response['task_photos'] = [];
        $response['anomaly_list'] = [];
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * start selected task
     * @param Request $request
     * @return array
     */
    public function start_task(Request $request) {
        $user_id = $request->get('user_id');
        $task_id = $request->get('task_id');

        $is_pending = Task::find($task_id)['task_state'];//check whether this task is in progress by another person
        if($is_pending && $is_pending != 0) {
            $response = Service::make_resp( "fail" , 204, "This task is in progress now by another person");
        } else {
            $task_started_at = Carbon::now();
            $task_state = 1;
            $result = Task::where('id', $task_id)
                ->update(compact('user_id', 'task_state', 'task_started_at'));
            if($result) {
                $response = Service::make_resp( "success" , 103, "You started this task.");
            } else {
                $response = Service::make_resp( "fail" , 200, "Unknown error");
            }

        }
        return $response;
    }

    /**
     * finish started task
     * @param Request $request
     * @return array
     */
    public function finish_task(Request $request) {
        $user_id = $request->get('user_id');
        $task_id = $request->get('task_id');

        $is_pending = Task::find($task_id);//check whether this task is in progress or not
        if($is_pending) {
            $task_finished_at = Carbon::now();
            $task_state = 2;
            $result = Task::where('id', $task_id)
                ->update(compact('task_state', 'task_finished_at'));
            if($result) {
                $response = Service::make_resp( "success" , 104, "You finished this task.");
            } else {
                $response = [
                    'status'=>'fail',
                    'code'=>206,
                    'message'=>"Finishing task error."];
            }
        } else {
            $response = Service::make_resp( "fail" , 200, "Unknown error");
        }
        return $response;
    }

    /**
     * register photo of started task
     * @param Request $request
     * @return array
     */
    public function post_task_photo(Request $request) {
        $task_id = $request->get('task_id');

        $task_photo_url = $request->get('task_photo');
        $photo_type = $request->get('photo_type');
        $task_photo = new TaskPhoto();
        $task_photo->task_id = $task_id;
        $task_photo->photo_url = $task_photo_url;
        $task_photo->photo_type = $photo_type;
        $task_photo->save();

        $now = Carbon::now()->format("Y-m-d h:i:s");
        $response = Service::make_resp( "success" , 100, $now);
        return $response;
    }

    /**
     * set anomalies of specified task
     * @param Request $request
     * @return array
     */
    public function set_anomaly(Request $request) {
        $user_id = $request->get('user_id');
        $task_id = $request->get('task_id');

        //anomalies which was set for this task before.
        $old_anomaly_ids = TaskAnomaly::where('task_id', $task_id)->select('anomaly_id')->get();

        $exist_anomaly_ids = [];
        foreach ($old_anomaly_ids as $old_anomaly_id) {
            $exist_anomaly_ids[] = $old_anomaly_id->anomaly_id;
        }
        $anomaly_ids = []; //all checked anomaly ids from app
        $i = 0;
        while (1) {
            $key = "anomaly_id_".$i;
            $anomaly_id = $request->get($key);
            if($anomaly_id) {
                array_push($anomaly_ids, $anomaly_id);
            } else {
                break;
            }
            $i++;
        }

        //anomalies which is to be added in the TaskAnomaly table
        $to_add_anomalies = array_values(array_diff($anomaly_ids, $exist_anomaly_ids));

        //anomalies which is to be deleted from the TaskAnomaly table
        $to_delete_anomalies = array_values(array_diff($exist_anomaly_ids, $anomaly_ids));

        $add_result = true;
        $delete_result = true;
        foreach ($to_add_anomalies as $to_add_anomaly) {
            $result1 = TaskAnomaly::create([
                'user_id'=>$user_id,
                'task_id'=>$task_id,
                'anomaly_id'=>$to_add_anomaly
            ]);
            $result1? : $add_result = false;
        }

        foreach ($to_delete_anomalies as $to_delete_anomaly) {
            $result2 = TaskAnomaly::where('anomaly_id', $to_delete_anomaly)
                ->where('task_id', $task_id)
                ->update(['task_id'=>null]);
            $result2? : $delete_result = false;
        }

        if($add_result && $delete_result) {
            $response = Service::make_resp( "success" , 105, "Anomaly was set.");
        } else {
            $response = Service::make_resp( "fail" , 207, "unknown error");
        }
        return $response;
    }

}
