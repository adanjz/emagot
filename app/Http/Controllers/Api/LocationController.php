<?php

namespace App\Http\Controllers\Api;

use App\Http\Service;
use App\Models\UserLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    /**
     * set user's current location periodly
     * @param Request $request
     * @return array
     */
    public function register_location(Request $request) {
        $user_id = $request->get('user_id');
        $longitude = $request->get('long');
        $latitude = $request->get('lat');

        $location = UserLocation::where('user_id', $user_id)->get();

        if(Service::validate_data($location)) {
            $result = UserLocation::where('user_id', $user_id)
                ->update(compact('user_id', 'longitude', 'latitude'));
        } else {
            $result = UserLocation::create([
                'user_id' => $user_id,
                'longitude' => $longitude,
                'latitude' => $latitude
            ]);
        }

        if($result) {
            $response = Service::make_resp( "success" , 100, "location updated");
        } else {
            $response = Service::make_resp( "fail" , 200, "location update error");
        }
        return $response;
    }

}
