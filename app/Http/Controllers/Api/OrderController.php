<?php

namespace App\Http\Controllers\Api;

use App\Http\Service;
use App\Models\ProceededOrder;
use App\Models\ProcessOrder;
use App\Models\Stock;
use App\Models\StockItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\User;

class OrderController extends Controller
{
    /**
     * get pending order of current user
     * he can know what orders he made so far
     * @param Request $request
     * @return array
     */

    public function get_pending_orders(Request $request) {
        $order_type = $request->get('order_type');
        $user_id = $request->get('user_id');

        $orders = ProcessOrder::where("sender_id", $user_id)
            ->where('order_type', $order_type)
            ->where('order_result', '!=', 2)
            ->orderBy('updated_at', 'desc')
            ->get();

        if(Service::validate_data($orders)) {
            foreach($orders as $order) {
                $user = User::find($order["receiver_id"]);
                $item = StockItem::find($order['item_id']);
                $order->order_id = $order["id"];
                $order->user_name = $user['name'];
                $order->item_name = $item['item_name'];
                $order->item_image = $item['item_image'];
            }
            $response = Service::make_resp( "success" , 100, "get pending orders");
            $response['pendingOrders'] = $orders;
        } else {
            $response = Service::make_resp( "fail" , 200, "no pending orders");
            $response['pendingOrders'] = [];
        }

        return $response;
    }


    /**
     * mark as check so that don't display pended orders next time.
     * @param $user_id
     * @param $order_id
     * @return array
     * @internal param $order_type
     */
    public function set_check_pended_order($user_id, $order_id) {
        ProcessOrder::where('id', $order_id)
            ->update(['order_result' => 2]);
        $response = Service::make_resp("success", 100, "success");
        return $response;
    }
    /**
     * get orders to let current user to confirm
     * @param $user_id
     * @return array
     */

    public function get_confirm_orders($user_id) {
        $orders = ProcessOrder::where('receiver_id', $user_id)
            ->where('order_result', 0)
            ->get();
        if(Service::validate_data($orders)) {
            foreach ($orders as $order) {
                $user = User::find($order["sender_id"]);
                $item = StockItem::find($order['item_id']);
                $order->user_name = $user['name'];
                $order->item_name = $item['item_name'];
                $order->item_image = $item['item_image'];
            }
            $response = Service::make_resp( "success" , 100, "get pending orders");
            $response['orders'] = $orders;
        } else {
            $response = Service::make_resp( "fail" , 200, $user_id);
            $response['orders'] = null;
        }
        return $response;
    }

    /**
     * make order: send or request order
     * @param Request $request
     * @return array
     */
    public function process_order(Request $request) {
        $sender_id = $request->get('sender_id'); // User id who send this orders
        $receiver_id = $request->get('receiver_id'); // User id who wil be accept this orders
        $order_type = $request->get('order_type'); //Send or Request order from user. 0:send, 1:request

        $fcm_token = User::find($receiver_id)['fcm_token'];
        $sender = User::find($sender_id)['name'];
        $i = 0;
        $result = true;
        $data = [];

        if($fcm_token == null) {
            return Service::make_resp( "failed" , 200, "no_fcm");
        }
        while(1) {
            $key_id = "id_".$i;
            $key_user = "item_user_".$i;
            $key_item_id = "item_id_".$i;
            $key_amount = "item_amount_".$i;
            $key_image = "item_image_".$i;
            $key_name = "item_name_".$i;
            $key_date = "item_date_".$i;
            $key_order_type = "item_order_type_".$i;

            $item_id = $request->get($key_item_id);
            $item_amount = $request->get($key_amount);
            $item_image = StockItem::find($item_id)['item_image'];
            $item_name = StockItem::find($item_id)['item_name'];
            if($item_id) {
                $result0 = ProcessOrder::create([
                    'sender_id' => $sender_id,
                    'receiver_id' => $receiver_id,
                    'item_id' => $item_id,
                    'item_amount' => $item_amount,
                    'order_type' => $order_type
                ]);

//                array_push($data, [
//                    $key_id => $result0->id,
//                    $key_user => $sender,
//                    $key_item_id => $item_id,
//                    $key_amount => $item_amount,
//                    $key_image => $item_image,
//                    $key_name => $item_name,
//                    $key_order_type => $order_type,
//                    $key_date => $result0->updated_at->format("Y-m-d h:i:s")
//                ]);
                $result = $result0 ? $result : $result0;
            } else {
                break;
            }
            $i++;
        }
        if($result) {
            $msg_body = $order_type == 0 ? $sender." sent new order": $sender." requested new order";
            if($this->send_notification($sender_id,$sender,  $order_type, "EMA-GOT", $msg_body, $data, $fcm_token, 0)) {
                $response = Service::make_resp( "success" , 100, "process order");
            } else {
                $response = Service::make_resp( "failed" , 200, "process order failed");
            }
        } else {
            $response = Service::make_resp( "fail" , 200, "process order");
        }

        return $response;

    }

    public function accept_order(Request $request) {
        $user_id = $request->get('receiver_id');
        $user = User::find($user_id)['name'];
        $i = 0;
        $user_ids = [];
        $order_result_sent_from_me = [];// make decrement to my stock, when he requested to me
        $order_result_sent_to_me = []; //make increment to my stock, when I requested
        $result = 1;
        while(1) {
            $key_id = "id_".$i;
            $key_amount = "item_amount_".$i;

            $result0 = 0;
            $id = $request->get($key_id);
            $item_amount = $request->get($key_amount);
            if($id) {

                $record = ProcessOrder::find($id);
                array_push($user_ids, $record->sender_id);//ids for sending notification
                if($record->order_type == 0) {//when the current user received items from another

                    /* check if user who sent order has items in his stock */
                    $sender_stock = Stock::where('user_id', $record->sender_id)
                        ->where('item_id', $record->item_id)->get();

                    if(Service::validate_data($sender_stock)) {
                        $reasonable_amount = $sender_stock[0]->item_amount;
                        $real_item_amount = min($item_amount, $reasonable_amount);

//                        return compact('reasonable_amount', 'real_item_amount');

                        Stock::where('user_id', $record->sender_id)
                            ->where('item_id', $record->item_id)
                            ->decrement('item_amount', $real_item_amount);
                    }


                    /** check if user already have current stock item in his stock
                    *if yes, increase item amount, otherwise create new stock item and add this
                    **/
                    if($this->checkIfStockExist($record->receiver_id, $record->item_id)) {
                        Stock::where('user_id', $record->receiver_id)
                            ->where('item_id', $record->item_id)
                            ->increment('item_amount', $item_amount);
                    } else {
                        Stock::create([
                           'user_id' => $record->receiver_id,
                            'item_id' => $record->item_id,
                            'item_amount' => $item_amount
                        ]);
                    }
                    $result0 = ProcessOrder::where('id', $id)
                        ->update([
                            'item_checked_amount' => $item_amount,
                            'order_result' => 1
                        ]);

                    /* make order result report for user who accepted this order */
                    $order_result_sent_to_me[] =
                        $this->make_order_report(
                            $record->sender_id,
                            $record->item_id,
                            $record->item_amount,
                            $item_amount);
                } else { //when the current user received order so that he sent item to another who made this order

                    /* check if user who are going to accept order has items in his stock */
                    $receiver_stock = Stock::where('user_id', $record->receiver_id)
                        ->where('item_id', $record->item_id)->get();
                    $result_0 = 0;
                    $real_item_amount = 0;
                    if(Service::validate_data($receiver_stock)) {
                        $reasonable_amount = $receiver_stock[0]->item_amount;
                        $real_item_amount = min($item_amount, $reasonable_amount);

//                        return compact('reasonable_amount', 'real_item_amount');
                        $result_0 = Stock::where('user_id', $record->receiver_id)
                            ->where('item_id', $record->item_id)
                            ->decrement('item_amount', $real_item_amount);
                    }
                    if($result_0 > 0) {
                        if($this->checkIfStockExist($record->sender_id, $record->item_id)) {
                            Stock::where('user_id', $record->sender_id)
                                ->where('item_id', $record->item_id)
                                ->increment('item_amount', $real_item_amount);
                        } else {
                            Stock::create([
                                'user_id' => $record->sender_id,
                                'item_id' => $record->item_id,
                                'item_amount' => $real_item_amount
                            ]);
                        }
                        $result0 = ProcessOrder::where('id', $id)
                            ->update([
                                'item_checked_amount' => $real_item_amount,
                                'order_result' => 1
                            ]);

                        /* make order result report for user who accepted this order */
                        $order_result_sent_from_me[] =
                            $this->make_order_report(
                                $record->sender_id,
                                $record->item_id,
                                $record->item_amount,
                                $real_item_amount);
                    }
                }
                $result = ($result0 > 0) ? $result : $result0;
                ProceededOrder::create(['order_id' => $id, 'user_id' => $record->sender_id]);
            } else {
                break;
            }
            $i++;
        }

        /* send notification to user who made this order before so that he can know whether orders were proceeded or not */
        $this->send_result_notification($user_ids, $user, 1);

        if($result > 0) {
            $response = Service::make_resp( "success" , 100, "successfully proceeded");
        } else {
            $response = Service::make_resp( "fail" , 200, "processing error");
        }
        $response['order_result_sent_from_me'] = $order_result_sent_from_me;
        $response['order_result_sent_to_me'] = $order_result_sent_to_me;
        return $response;
    }

    /**
     * get all accepted orders to get information about which orders were accepted
     * @param $user_id
     * @return array
     */
    public function get_proceeded_orders($user_id) {
        $orders = ProceededOrder::where('user_id', $user_id)
            ->where('check_result', 0)
            ->get();
        $order_result_sent_from_me = [];// make decrement to my stock, when he requested to me
        $order_result_sent_to_me = []; //make increment to my stock, when I requested
        if(Service::validate_data($orders)) {
            foreach ($orders as $order) {
                $id = $order->id;
                $user_name = $order->order->user->name;
                $item_name = $order->order->item->item_name;
                $total_item_amount = $order->order->item_amount;
                $real_item_amount = $order->order->item_checked_amount;
                $order_type = $order->order->order_type;
                if($order_type == 0) {
                    $order_result_sent_from_me[] = compact('id','user_name', 'item_name', 'total_item_amount', 'real_item_amount');
                } else {
                    $order_result_sent_to_me[] = compact('id', 'user_name', 'item_name', 'total_item_amount', 'real_item_amount');
                }
            }

            $response = Service::make_resp( "success" , 100, "all proceeded orders");
        } else {
            $response = Service::make_resp( "fail" , 200, "fetching error");
        }

        $response['order_result_sent_from_me'] = $order_result_sent_from_me;
        $response['order_result_sent_to_me'] = $order_result_sent_to_me;
        return $response;
    }

    /**
     * mark check to proceeded orders which user checked
     * @param $user_id
     * @internal param Request $request
     */
    public function set_check_proceeded_orders($user_id) {
        ProceededOrder::where('user_id', $user_id)->update(['check_result' => 1]);
    }

    /** check if user already have current stock item in his stock
     * @param $user_id
     * @param $item_id
     * @return bool
     */
    public function checkIfStockExist($user_id, $item_id) {
        $stock = Stock::where('user_id', $user_id)
            ->where('item_id', $item_id)->get();
        return Service::validate_data($stock);
    }

    /**
     * make order report for user who accept orders
     * @param $user_id
     * @param $item_id
     * @param $total_item_amount
     * @param $real_item_amount
     * @return array
     */
    public function make_order_report($user_id, $item_id, $total_item_amount, $real_item_amount) {
        $user_name = User::find($user_id)->name;
        $item_name = StockItem::find($item_id)->item_name;

        return compact('user_name', 'item_name', 'total_item_amount', 'real_item_amount');
    }

    /**
     * send notification to user to let him see and accept order
     * @param $user_id
     * @param $user_name
     * @param $order_type
     * @param string $msg
     * @param string $body
     * @param array $data
     * @param $fcm_token
     * @param $notification_type
     * @return bool
     */
    public function send_notification($user_id, $user_name, $order_type, $msg = "", $body = "", $data = [], $fcm_token, $notification_type) {

        // notification_type: 0: send or request , 1: accept order
        $token = $fcm_token;
        if($token == null) {
            return false;
        }
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

//        $notificationBuilder = new PayloadNotificationBuilder();
//        $notificationBuilder->setBody($body)
//            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();

        $dataBuilder->addData(compact('user_id', 'user_name', 'order_type', 'notification_type'));

        $option = $optionBuilder->build();
//        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        $status = $downstreamResponse->numberSuccess();
        return $status == 1;
    }

    /**
     * send notification to user who make current order, so that he can get whether his orders were proceeded or not
     * @param $ids
     * @param $sender_name
     * @param $notification_type
     */
    public function send_result_notification($ids, $sender_name, $notification_type) {

        $ids = array_unique($ids);
        $fcm_tokens = [];
        foreach ($ids as $id) {
            $fcm_token = User::find($id)['fcm_token'];
            array_push($fcm_tokens,$fcm_token );
        }

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $msg = $sender_name." accepted your order";
//        $notificationBuilder = new PayloadNotificationBuilder();
//        $notificationBuilder->setBody($msg)
//            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();

        $option = $optionBuilder->build();
//        $notification = $notificationBuilder->build();
        $dataBuilder->addData(['notification_type' => $notification_type]);
        $data = $dataBuilder->build();


        foreach ($fcm_tokens as $fcm_token) {

            if($fcm_token != null) {
                $downstreamResponse = FCM::sendTo($fcm_token, $option, null, $data);

                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();
            }
        }
    }

    /**
     * test sending notificaiton
     * @param $msg
     */
    public function send_msg($msg) {
        $fcm_tokens = User::where('fcm_token', '!=', '')->select('fcm_token')->get();
        echo($fcm_tokens[0]['fcm_token']);

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('ema-got');
        $notificationBuilder->setBody($msg)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();

        $dataBuilder->addData([
            'user_id' => 1,
            'order_type' => 1,
            'notification_type' => 1
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();


        foreach ($fcm_tokens as $fcm_token) {
            $token = $fcm_token['fcm_token'];

            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();
        }
    }
}
