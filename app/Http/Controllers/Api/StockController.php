<?php

namespace App\Http\Controllers\Api;

use App\Http\Service;
use App\Models\Medidores;
use App\Models\MedidoresUsuarios;
use App\Models\MedidorLogs;
use App\Models\ProcessOrder;
use App\Models\Stock;
use App\Models\StockItem;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockController extends Controller
{

    /**
     * get user stock info with stock items.
     * @param $id
     * @return array
     */
    public function index($id) {
        $stocks = Stock::where('user_id', $id)->select('id', 'item_amount', 'item_id')->get();
        if(Service::validate_data($stocks)) {
            foreach ($stocks as $stock) {
                $stock['item_name'] = $stock->stock_item->item_name;
            }
            $response = Service::make_resp( "success" , 106, "my stock");
            $response['stock'] = $stocks;
        } else {
            $response = Service::make_resp( "success" , 203, "no stock");
            $response['stock'] = [];
        }

        return $response;
    }

    /**
     * get stock item info and item amount of current user with barcode
     * @param $barcode
     * @param $id
     * @return array
     */
    public function search_item($barcode, $id) {
        $stock_item = StockItem::where('item_barcode', $barcode)->get();
        if(Service::validate_data($stock_item)) {
            $user_item = Stock::where('user_id', $id)
                ->where('item_id', $stock_item[0]->id)->select('item_amount')
                ->get();
            $user_item_sent_pending = ProcessOrder::where("sender_id", $id)
                ->where("item_id", $stock_item[0]->id)
                ->where("order_result", 0)
                ->sum("item_amount");

            if(Service::validate_data($user_item)) {
                $stock_item[0]->user_item = $user_item[0]->item_amount - $user_item_sent_pending;
            } else {
                $stock_item[0]->user_item = 0;
            }
            $response = Service::make_resp( "success" , 107, "success");
            $response['stock_item'] = $stock_item[0];
        } else {
            $response = Service::make_resp( "fail" , 209, "no item");
            $response['stock_item'] = null;
        }
        return $response;
    }

    /**
     * process user stock item (decrease item mount as user consumed on a specific task
     * @param Request $request
     * @return array
     */
    public function check_used_item(Request $request) {
        $user_id = $request->get('user_id');

        $i = 0;
        $data = $request->all();
        foreach ($data['items'] as $item) {
            $item_id = $item['item_id'];
            $item_amount = $item['item_amount'];
            if ($item_id) {
                $old_item_amount = Stock::where('user_id', $user_id)
                    ->where('item_id', $item_id)->select('item_amount')->first();
                if(!empty($old_item_amount)){
                    Stock::where('user_id', $user_id)
                        ->where('item_id', $item_id)
                        ->update([
                            'item_amount' => $old_item_amount->item_amount - $item_amount
                        ]);
                }


            } else {
                break;
            }
            $i++;
        }
        foreach($data['medidores'] as $medidores){
            $medidorInput = Medidores::where('serie',$medidores['inputSerie'])->first();
            $medidorOutput = Medidores::where('serie',$medidores['outputSerie'])->first();
            if(!empty($medidorOutput)){
                $medidorOutput->lectura = $medidores['outputLectura'];
                $medidorOutput->save();

                $medidorUsuario = MedidoresUsuarios::where('medidor',$medidorOutput->id)->first();
                $medidorUsuario->user_id = 0;
                $medidorUsuario->save();

                $medLogO = new MedidorLogs();
                $medLogO->medidor = $medidorOutput->id;
                $medLogO->estado = 4;
                $medLogO->descripcion = 'Medidor retirado';
                $medLogO->save();

            }
            if(!empty($medidorInput)) {
                $medLogI = new MedidorLogs();
                $medLogI->medidor = $medidorInput->id;
                $medLogI->estado = 3;
                $medLogI->descripcion = 'Medidor instalado en direccion';
                $medLogI->save();
            }


        }

        $response = Service::make_resp( "success" , 107, "success");
        return $response;

    }
}
