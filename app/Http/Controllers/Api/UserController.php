<?php

namespace App\Http\Controllers\Api;

use App\Http\Service;
use App\Models\LogedinUser;
use App\Models\Medidores;
use App\Models\MedidoresUsuarios;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use TheSeer\Tokenizer\Token;

class UserController extends Controller
{
    private $validator;
    public function __construct()
    {
        $this->validator = new Service();
    }

    /**
     * get user list except current user (used when make order)
     * @param $user_id
     * @return array
     */
    public function index($user_id) {
        $users = User::where('id', '!=', $user_id)
            ->where('allow', 1)
            ->orderBy('name', 'asc')
            ->get();
        if($this->validator->validate_data($users)) {
            $response = Service::make_resp( "success" , 108, "all users except me");
            $response['users'] = $users;
        } else {
            $response = Service::make_resp( "fail" , 210, "no users");
            $response['users'] = $users;
        }
        return $response;
    }

    /**
     * register user face so that user can facial login afterwards
     * @param Request $request
     * @return array
     */
    public function register(Request $request) {
        $name = $request->get('user_name');
        $fcm_token  = $request->get('fcm_token');
        $file = $request->file('face_id');
        $destinationPath = config('ema-got.face_id_dir');
        $face_id = $file->getClientOriginalName();
        if(!file_exists($destinationPath)) mkdir($destinationPath);
        $file->move($destinationPath, $face_id);

        $result = User::create([
            'name'=>$name,
            'face_id'=>$face_id,
            'fcm_token' => $fcm_token
        ]);
        if ($result) {
            $response = Service::make_resp( "success" , 101, "Register success");
        } else {
            $response = Service::make_resp( "fail" , 201, "Register failed");
        }
        return $response;
    }

    /**
     * get all registered faceids to login
     * @return array
     */
    public function get_face_ids() {
        $faces = User::where('face_id', '!=', "")->select('face_id')->get();
        if(Service::validate_data($faces)) {
            foreach ($faces as $face) {
                $face_ids[] = $face['face_id'];
            }
            $response = Service::make_resp( "success" , 100, "success");
            $response['face_ids'] = $face_ids;
        } else {
            $response = Service::make_resp( "fail" , 200, "no data");
            $response['face_ids'] = [];
        }

        return $response;
    }

    /**
     * download the face id files.
     * @param $file_name
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download_face_ids($file_name) {
        $file_path = public_path().'/Face-info'.'//'.$file_name;
        return response()->download($file_path, $file_name);
    }

    /**
     * user log in with his facial info
     * @param Request $request
     * @return array
     */
    public function login(Request $request) {
        $face_id = $request->get('file_name');
        $fcm_token  = $request->get('fcm_token');
        $imei = $request->get('imei');
        $user = User::where('face_id', $face_id)->get();

        if(Service::validate_data($user)) {

            $sign_state = LogedinUser::where('user_id', $user[0]->id)
                ->orderBy('updated_at', 'desc')
                ->get();

            $last_time_login = new Carbon($user[0]->updated_at);
            if (Service::validate_data($sign_state)) {

                if($sign_state[0]->sign_state == 1) { // in case of user already loged in.
                    if($sign_state[0]->imei == $imei) { // if loged in on same phone.
                        $new_token = Hash::make(str_random(32));

                        $pivot = ['id', $user[0]->id];
                        $data = [
                            'remember_token' => $new_token,
                            'fcm_token' => $fcm_token
                        ];
                        $this->update_user($pivot, $data);

                        $response = Service::make_resp( "success" , 102, $new_token);
                        $response['user'] = $user;
                        return $response;
                    } else { // in case of logged in other phone
                        if($this->is_pastday($last_time_login)) { // if loged in today or past day.
                            $new_token = Hash::make(str_random(32));

                            $pivot = ['id', $user[0]->id];
                            $data = [
                                'remember_token' => $new_token,
                                'fcm_token' => $fcm_token
                            ];
                            $this->update_user($pivot, $data);

                            $pivot = ['id', $sign_state[0]->id];
                            $data = ['sign_state' => 1, 'imei' => $imei];
                            $this->update_logedin($pivot, $data);
                            $response = Service::make_resp( "success" , 102, $new_token);
                            $response['user'] = $user;
                            return $response;
                        } else {
                            $response = Service::make_resp( "fail" , 211, "Login failed.You already signed in.");
                            $response['user'] = $user;
                        }
                    }
                } else { // new log in
                    $token = Hash::make(str_random(32));
                    $pivot = ['face_id', $face_id];
                    $data = [
                        'remember_token' => $token,
                        'fcm_token' => $fcm_token
                    ];
                    $this->update_user($pivot, $data);
                    $pivot = ['id', $sign_state[0]->id];
                    $data = ['sign_state' => 1, 'imei' => $imei];
                    $this->update_logedin($pivot, $data);

                    $response = Service::make_resp( "success" , 102, $token);
                    $response['user'] = $user;
                }

            } else {
                $token = Hash::make(str_random(32));
                $pivot = ['id', $user[0]->id];
                $data = ['remember_token' => $token,
                    'fcm_token' => $fcm_token];
                $this->update_user($pivot, $data);
                LogedinUser::create([
                    'user_id' => $user[0]->id,
                    'imei' => $imei,
                    'sign_state' => 1
                ]);
                $response = Service::make_resp( "success" , 102, $token);
                $response['user'] = $user;
            }

        } else {
            $response = Service::make_resp( "fail" , 200, "Login failed.Please register.");
            $response['user'] = [];
        }

        return $response;
    }

    /**
     * user log in with token in background
     * @param Request $request
     * @return array
     */
    public function token_login(Request $request) {
        $token = $request->get('token');
        $fcm_token = $request->get('fcm_token');
        $user = User::where('remember_token', $token)->get();
        if(Service::validate_data($user)) {
            $last_time_login = new Carbon($user[0]->updated_at);
            if($this->is_pastday($last_time_login)) {
                $pivot = ['id', $user[0]->id];
                $data = ['remember_token'=>"",
                    'fcm_token' => $fcm_token];
                $this->update_user($pivot, $data);

                $pivot = ['user_id', $user[0]->id];
                $data = ['sign_state' => 0];
                $this->update_logedin($pivot, $data);
            } else {
                $new_token = Hash::make(str_random(32));
                $pivot = ['id', $user[0]->id];
                $data = ['remember_token' => $new_token, 'fcm_token' => $fcm_token];
                $this->update_user($pivot, $data);

                $pivot = ['user_id', $user[0]->id];
                $data = ['sign_state' => 1];
                $this->update_logedin($pivot, $data);

                $response = Service::make_resp( "success" , 102, $new_token);
                $response['user'] = $user;
                return $response;
            }
        }
        $response = Service::make_resp( "fail" , 202, "");
        $response['user'] = [];

        return $response;
    }

    /**
     * user log out
     * @param Request $request
     * @return array
     */
    public function logout(Request $request) {
        $user_id = $request->get('user_id');
        $user = User::where("id", $user_id)->get();
        if(!Service::validate_data($user)) {
            $response = Service::make_resp( "fail" , 200, "logout fail");
        } else {
            User::where('id', $user_id)
                ->update(['remember_token'=>""]);
            LogedinUser::where('user_id', $user_id)
                ->update(['sign_state' => 0]);
            $response = Service::make_resp( "succcess" , 100, "logout success");
        }
        return $response;
    }

    /**
     * register firebase token to receive notification
     * @param Request $request
     */
    public function fcm_register(Request $request) {
        $fcm_token = $request->get('fcm_token');
        $id = $request->get('id');

        User::where('id', $id)->update(['fcm_token' => $fcm_token]);
    }

    /**
     * check if user logged in yesterday or  today
     * @param $last_login_time
     * @return bool
     */
    public function is_pastday($last_login_time) {
        $now = Carbon::now();
        if($last_login_time->diff($now)->days >= 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * update user info
     * @param $pivot : just pivot point (the phase to be used as `where $pivot[0] = $pivot[1]` in db query)
     * @param $data
     */
    public function update_user($pivot, $data) {
        User::where($pivot[0], $pivot[1])
            ->update($data);
    }

    /**
     * update user loged in table by setting login state of user
     * @param $pivot
     * @param $data
     */
    public function update_logedin($pivot, $data) {
        LogedinUser::where($pivot[0], $pivot[1])
            ->update($data);
    }

    public function getMedidores($user_id) {
        $ids = MedidoresUsuarios::select('medidor')->where('user_id',$user_id)->get()->pluck('medidor')->toArray();
        $medidores = Medidores::select('serie')->whereIn('id',$ids)->get();
        $response = Service::make_resp( "success" , 200, "medidores");
        $response['medidores'] = $medidores;
        return $response;
    }
}
