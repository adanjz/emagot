<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMedidoresRequest;
use App\Http\Requests\UpdateMedidoresRequest;
use App\Models\Medidores;
use App\Models\MedidoresUsuarios;
use App\Models\MedidorLogs;
use App\Models\User;
use App\Models\Users;
use App\Repositories\MedidoresRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MedidoresController extends AppBaseController
{
    /** @var  MedidoresRepository */
    private $medidoresRepository;

    public function __construct(MedidoresRepository $medidoresRepo)
    {
        $this->medidoresRepository = $medidoresRepo;
    }

    /**
     * Display a listing of the Medidores.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->medidoresRepository->pushCriteria(new RequestCriteria($request));
        $medidores = $this->medidoresRepository->all();

        return view('medidores.index')
            ->with('medidores', $medidores);
    }

    /**
     * Show the form for creating a new Medidores.
     *
     * @return Response
     */
    public function create()
    {
        return view('medidores.create');
    }

    /**
     * Store a newly created Medidores in storage.
     *
     * @param CreateMedidoresRequest $request
     *
     * @return Response
     */
    public function store(CreateMedidoresRequest $request)
    {
        $input = $request->all();

//        $medidores = $this->medidoresRepository->create($input);
        for($i=$input['rango_d'];$i<$input['rango_h']+1;$i++){
            if(empty(Medidores::where('modelo',$input['marca'])->where('serie',$i)->first())){
                $medidorNuevo = new Medidores();
                $medidorNuevo->modelo = $input['marca'];
                $medidorNuevo->serie = $input['prefijo'].$i;
                $medidorNuevo->lectura = 0;
                $medidorNuevo->save();
                $log = new MedidorLogs();
                $log->medidor = $medidorNuevo->id;
                $log->estado = 1;
                $log->descripcion = 'Nuevo medidor ingresado';
                $log->save();
            }
        }
        Flash::success('Medidores saved successfully.');
        return redirect(route('medidores.index'));
    }

    /**
     * Display the specified Medidores.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $medidores = $this->medidoresRepository->findWithoutFail($id);

        if (empty($medidores)) {
            Flash::error('Medidores not found');

            return redirect(route('medidores.index'));
        }

        return view('medidores.show')->with('medidores', $medidores);
    }

    /**
     * Show the form for editing the specified Medidores.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $medidores = $this->medidoresRepository->findWithoutFail($id);

        if (empty($medidores)) {
            Flash::error('Medidores not found');

            return redirect(route('medidores.index'));
        }

        return view('medidores.edit')->with('medidores', $medidores);
    }

    /**
     * Update the specified Medidores in storage.
     *
     * @param  int              $id
     * @param UpdateMedidoresRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedidoresRequest $request)
    {
        $medidores = $this->medidoresRepository->findWithoutFail($id);

        if (empty($medidores)) {
            Flash::error('Medidores not found');

            return redirect(route('medidores.index'));
        }

        $medidores = $this->medidoresRepository->update($request->all(), $id);

        Flash::success('Medidores updated successfully.');

        return redirect(route('medidores.index'));
    }

    /**
     * Remove the specified Medidores from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $medidores = $this->medidoresRepository->findWithoutFail($id);

        if (empty($medidores)) {
            Flash::error('Medidores not found');

            return redirect(route('medidores.index'));
        }

        $this->medidoresRepository->delete($id);

        Flash::success('Medidores deleted successfully.');

        return redirect(route('medidores.index'));
    }
    public function assignTo(Request $request,$id){
        $medidor = Medidores::find($id);
        $usuario = Users::find($request->get('legajo_id'));
        if(empty(MedidorLogs::where('estado',2)->where('medidor',$id)->first())){
            $log = new MedidorLogs();
            $log->estado = 2;
            $log->medidor = $id;
            $log->descripcion = 'Asignado a usuario: '.$usuario->name;
            $log->save();
        }
        $mu = new MedidoresUsuarios();
        $mu->medidor = $id;
        $mu->user_id = $usuario->id;
        $mu->save();

        Flash::success('Medidor asignado.');
        return redirect(route('medidores.index'));

    }
    public function asignarMultiples(Request $request){
        $data = $request->all();
        $usuario = User::find($data['legajo']);
        foreach($data['medidores'] as $mid){
            if(empty(MedidorLogs::where('estado',2)->where('medidor',$mid)->first())){
                $log = new MedidorLogs();
                $log->estado = 2;
                $log->medidor = $mid;
                $log->descripcion = 'Asignado a usuario: '.$usuario->name;
                $log->save();
            }
            $mu = new MedidoresUsuarios();
            $mu->medidor = $mid;
            $mu->user_id = $usuario->id;
            $mu->save();

        }
    }

    /**
     * Change state the specified Medidores in storage.
     *
     * @param  int              $id
     * @param int $state
     *
     * @return Response
     */
    public function changeState($id, $state)
    {
        $medidor_logs = MedidorLogs::where('medidor',$id)->orderBy('id','desc')->first();
        $medidor_logs->estado = $state;
        $medidor_logs->save();

        Flash::success('Medidores updated successfully.');

        return redirect(route('medidores.index'));
    }
}
