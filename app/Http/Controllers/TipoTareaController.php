<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTipoTareaRequest;
use App\Http\Requests\UpdateTipoTareaRequest;
use App\Repositories\TipoTareaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TipoTareaController extends AppBaseController
{
    /** @var  TipoTareaRepository */
    private $tipoTareaRepository;

    public function __construct(TipoTareaRepository $tipoTareaRepo)
    {
        $this->tipoTareaRepository = $tipoTareaRepo;
    }

    /**
     * Display a listing of the TipoTarea.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tipoTareaRepository->pushCriteria(new RequestCriteria($request));
        $tipoTareas = $this->tipoTareaRepository->all();

        return view('tipo_tareas.index')
            ->with('tipoTareas', $tipoTareas);
    }

    /**
     * Show the form for creating a new TipoTarea.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_tareas.create');
    }

    /**
     * Store a newly created TipoTarea in storage.
     *
     * @param CreateTipoTareaRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoTareaRequest $request)
    {
        $input = $request->all();

        $tipoTarea = $this->tipoTareaRepository->create($input);

        Flash::success('Tipo Tarea saved successfully.');

        return redirect(route('tipoTareas.index'));
    }

    /**
     * Display the specified TipoTarea.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoTarea = $this->tipoTareaRepository->findWithoutFail($id);

        if (empty($tipoTarea)) {
            Flash::error('Tipo Tarea not found');

            return redirect(route('tipoTareas.index'));
        }

        return view('tipo_tareas.show')->with('tipoTarea', $tipoTarea);
    }

    /**
     * Show the form for editing the specified TipoTarea.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoTarea = $this->tipoTareaRepository->findWithoutFail($id);

        if (empty($tipoTarea)) {
            Flash::error('Tipo Tarea not found');

            return redirect(route('tipoTareas.index'));
        }

        return view('tipo_tareas.edit')->with('tipoTarea', $tipoTarea);
    }

    /**
     * Update the specified TipoTarea in storage.
     *
     * @param  int              $id
     * @param UpdateTipoTareaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoTareaRequest $request)
    {
        $tipoTarea = $this->tipoTareaRepository->findWithoutFail($id);

        if (empty($tipoTarea)) {
            Flash::error('Tipo Tarea not found');

            return redirect(route('tipoTareas.index'));
        }

        $tipoTarea = $this->tipoTareaRepository->update($request->all(), $id);

        Flash::success('Tipo Tarea updated successfully.');

        return redirect(route('tipoTareas.index'));
    }

    /**
     * Remove the specified TipoTarea from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoTarea = $this->tipoTareaRepository->findWithoutFail($id);

        if (empty($tipoTarea)) {
            Flash::error('Tipo Tarea not found');

            return redirect(route('tipoTareas.index'));
        }

        $this->tipoTareaRepository->delete($id);

        Flash::success('Tipo Tarea deleted successfully.');

        return redirect(route('tipoTareas.index'));
    }
}
