<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     */
    public function index()
    {
        return redirect('/tareas');

    }

    public function test(Request $request) {

        $file = $request->file('face_id');
        if ($file) {
            $myfile1 = fopen("newfile.txt", "w") or die("Unable to open file!");
            $text = $file->getClientOriginalName()."____".$request->get('user_name');
            fwrite($myfile1, $text);
            fclose($myfile1);
        }
        $destinationPath = 'Face-info';
        if(!file_exists($destinationPath)) mkdir($destinationPath);
        $file->move($destinationPath,$file->getClientOriginalName());

        $result = ['status'=>'success', 'code'=>101, 'message'=>$text];
        return $result;
    }
}
