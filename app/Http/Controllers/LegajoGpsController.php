<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLegajoGpsRequest;
use App\Http\Requests\UpdateLegajoGpsRequest;
use App\Repositories\LegajoGpsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class LegajoGpsController extends AppBaseController
{
    /** @var  LegajoGpsRepository */
    private $legajoGpsRepository;

    public function __construct(LegajoGpsRepository $legajoGpsRepo)
    {
        $this->legajoGpsRepository = $legajoGpsRepo;
    }

    /**
     * Display a listing of the LegajoGps.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->legajoGpsRepository->pushCriteria(new RequestCriteria($request));
        $legajoGps = $this->legajoGpsRepository->all();

        return view('legajo_gps.index')
            ->with('legajoGps', $legajoGps);
    }

    /**
     * Show the form for creating a new LegajoGps.
     *
     * @return Response
     */
    public function create()
    {
        return view('legajo_gps.create');
    }

    /**
     * Store a newly created LegajoGps in storage.
     *
     * @param CreateLegajoGpsRequest $request
     *
     * @return Response
     */
    public function store(CreateLegajoGpsRequest $request)
    {
        $input = $request->all();

        $legajoGps = $this->legajoGpsRepository->create($input);

        Flash::success('Legajo Gps saved successfully.');

        return redirect(route('legajoGps.index'));
    }

    /**
     * Display the specified LegajoGps.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $legajoGps = $this->legajoGpsRepository->findWithoutFail($id);

        if (empty($legajoGps)) {
            Flash::error('Legajo Gps not found');

            return redirect(route('legajoGps.index'));
        }

        return view('legajo_gps.show')->with('legajoGps', $legajoGps);
    }

    /**
     * Show the form for editing the specified LegajoGps.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $legajoGps = $this->legajoGpsRepository->findWithoutFail($id);

        if (empty($legajoGps)) {
            Flash::error('Legajo Gps not found');

            return redirect(route('legajoGps.index'));
        }

        return view('legajo_gps.edit')->with('legajoGps', $legajoGps);
    }

    /**
     * Update the specified LegajoGps in storage.
     *
     * @param  int              $id
     * @param UpdateLegajoGpsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLegajoGpsRequest $request)
    {
        $legajoGps = $this->legajoGpsRepository->findWithoutFail($id);

        if (empty($legajoGps)) {
            Flash::error('Legajo Gps not found');

            return redirect(route('legajoGps.index'));
        }

        $legajoGps = $this->legajoGpsRepository->update($request->all(), $id);

        Flash::success('Legajo Gps updated successfully.');

        return redirect(route('legajoGps.index'));
    }

    /**
     * Remove the specified LegajoGps from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $legajoGps = $this->legajoGpsRepository->findWithoutFail($id);

        if (empty($legajoGps)) {
            Flash::error('Legajo Gps not found');

            return redirect(route('legajoGps.index'));
        }

        $this->legajoGpsRepository->delete($id);

        Flash::success('Legajo Gps deleted successfully.');

        return redirect(route('legajoGps.index'));
    }
}
