<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMaterialTypesRequest;
use App\Http\Requests\UpdateMaterialTypesRequest;
use App\Repositories\MaterialTypesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MaterialTypesController extends AppBaseController
{
    /** @var  MaterialTypesRepository */
    private $materialTypesRepository;

    public function __construct(MaterialTypesRepository $materialTypesRepo)
    {
        $this->materialTypesRepository = $materialTypesRepo;
    }

    /**
     * Display a listing of the MaterialTypes.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->materialTypesRepository->pushCriteria(new RequestCriteria($request));
        $materialTypes = $this->materialTypesRepository->all();

        return view('material_types.index')
            ->with('materialTypes', $materialTypes);
    }

    /**
     * Show the form for creating a new MaterialTypes.
     *
     * @return Response
     */
    public function create()
    {
        return view('material_types.create');
    }

    /**
     * Store a newly created MaterialTypes in storage.
     *
     * @param CreateMaterialTypesRequest $request
     *
     * @return Response
     */
    public function store(CreateMaterialTypesRequest $request)
    {
        $input = $request->all();

        $materialTypes = $this->materialTypesRepository->create($input);

        Flash::success('Material Types saved successfully.');

        return redirect(route('materialTypes.index'));
    }

    /**
     * Display the specified MaterialTypes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $materialTypes = $this->materialTypesRepository->findWithoutFail($id);

        if (empty($materialTypes)) {
            Flash::error('Material Types not found');

            return redirect(route('materialTypes.index'));
        }

        return view('material_types.show')->with('materialTypes', $materialTypes);
    }

    /**
     * Show the form for editing the specified MaterialTypes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $materialTypes = $this->materialTypesRepository->findWithoutFail($id);

        if (empty($materialTypes)) {
            Flash::error('Material Types not found');

            return redirect(route('materialTypes.index'));
        }

        return view('material_types.edit')->with('materialTypes', $materialTypes);
    }

    /**
     * Update the specified MaterialTypes in storage.
     *
     * @param  int              $id
     * @param UpdateMaterialTypesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMaterialTypesRequest $request)
    {
        $materialTypes = $this->materialTypesRepository->findWithoutFail($id);

        if (empty($materialTypes)) {
            Flash::error('Material Types not found');

            return redirect(route('materialTypes.index'));
        }

        $materialTypes = $this->materialTypesRepository->update($request->all(), $id);

        Flash::success('Material Types updated successfully.');

        return redirect(route('materialTypes.index'));
    }

    /**
     * Remove the specified MaterialTypes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $materialTypes = $this->materialTypesRepository->findWithoutFail($id);

        if (empty($materialTypes)) {
            Flash::error('Material Types not found');

            return redirect(route('materialTypes.index'));
        }

        $this->materialTypesRepository->delete($id);

        Flash::success('Material Types deleted successfully.');

        return redirect(route('materialTypes.index'));
    }
}
