<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFlotillasUsuariosRequest;
use App\Http\Requests\UpdateFlotillasUsuariosRequest;
use App\Repositories\FlotillasUsuariosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class FlotillasUsuariosController extends AppBaseController
{
    /** @var  FlotillasUsuariosRepository */
    private $flotillasUsuariosRepository;

    public function __construct(FlotillasUsuariosRepository $flotillasUsuariosRepo)
    {
        $this->flotillasUsuariosRepository = $flotillasUsuariosRepo;
    }

    /**
     * Display a listing of the FlotillasUsuarios.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->flotillasUsuariosRepository->pushCriteria(new RequestCriteria($request));
        $flotillasUsuarios = $this->flotillasUsuariosRepository->all();

        return view('flotillas_usuarios.index')
            ->with('flotillasUsuarios', $flotillasUsuarios);
    }

    /**
     * Show the form for creating a new FlotillasUsuarios.
     *
     * @return Response
     */
    public function create()
    {
        return view('flotillas_usuarios.create');
    }

    /**
     * Store a newly created FlotillasUsuarios in storage.
     *
     * @param CreateFlotillasUsuariosRequest $request
     *
     * @return Response
     */
    public function store(CreateFlotillasUsuariosRequest $request)
    {
        $input = $request->all();

        $flotillasUsuarios = $this->flotillasUsuariosRepository->create($input);

        Flash::success('Flotillas Usuarios saved successfully.');

        return redirect(route('flotillasUsuarios.index'));
    }

    /**
     * Display the specified FlotillasUsuarios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $flotillasUsuarios = $this->flotillasUsuariosRepository->findWithoutFail($id);

        if (empty($flotillasUsuarios)) {
            Flash::error('Flotillas Usuarios not found');

            return redirect(route('flotillasUsuarios.index'));
        }

        return view('flotillas_usuarios.show')->with('flotillasUsuarios', $flotillasUsuarios);
    }

    /**
     * Show the form for editing the specified FlotillasUsuarios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $flotillasUsuarios = $this->flotillasUsuariosRepository->findWithoutFail($id);

        if (empty($flotillasUsuarios)) {
            Flash::error('Flotillas Usuarios not found');

            return redirect(route('flotillasUsuarios.index'));
        }

        return view('flotillas_usuarios.edit')->with('flotillasUsuarios', $flotillasUsuarios);
    }

    /**
     * Update the specified FlotillasUsuarios in storage.
     *
     * @param  int              $id
     * @param UpdateFlotillasUsuariosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFlotillasUsuariosRequest $request)
    {
        $flotillasUsuarios = $this->flotillasUsuariosRepository->findWithoutFail($id);

        if (empty($flotillasUsuarios)) {
            Flash::error('Flotillas Usuarios not found');

            return redirect(route('flotillasUsuarios.index'));
        }

        $flotillasUsuarios = $this->flotillasUsuariosRepository->update($request->all(), $id);

        Flash::success('Flotillas Usuarios updated successfully.');

        return redirect(route('flotillasUsuarios.index'));
    }

    /**
     * Remove the specified FlotillasUsuarios from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $flotillasUsuarios = $this->flotillasUsuariosRepository->findWithoutFail($id);

        if (empty($flotillasUsuarios)) {
            Flash::error('Flotillas Usuarios not found');

            return redirect(route('flotillasUsuarios.index'));
        }

        $this->flotillasUsuariosRepository->delete($id);

        Flash::success('Flotillas Usuarios deleted successfully.');

        return redirect(route('flotillasUsuarios.index'));
    }
}
