<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAnomaliasRequest;
use App\Http\Requests\UpdateAnomaliasRequest;
use App\Models\Anomalias;
use App\Repositories\AnomaliasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AnomaliasController extends AppBaseController
{
    /** @var  AnomaliasRepository */
    private $anomaliasRepository;

    public function __construct(AnomaliasRepository $anomaliasRepo)
    {
        $this->anomaliasRepository = $anomaliasRepo;
    }

    /**
     * Display a listing of the Anomalias.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->anomaliasRepository->pushCriteria(new RequestCriteria($request));
        $anomaliases = $this->anomaliasRepository->all();

        return view('anomaliases.index')
            ->with('anomaliases', $anomaliases);
    }

    /**
     * Show the form for creating a new Anomalias.
     *
     * @return Response
     */
    public function create()
    {
        return view('anomaliases.create');
    }

    /**
     * Store a newly created Anomalias in storage.
     *
     * @param CreateAnomaliasRequest $request
     *
     * @return Response
     */
    public function store(CreateAnomaliasRequest $request)
    {
        $input = $request->all();

        if(empty(Anomalias::where('codigo',$request->get('codigo'))->first())){
            $anomalias = $this->anomaliasRepository->create($input);
            Flash::success('Anomalia guardada.');
        }else{
            Flash::success('Anomalia no guardada.');
        }




        return redirect(route('anomaliases.index'));
    }

    /**
     * Display the specified Anomalias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $anomalias = $this->anomaliasRepository->findWithoutFail($id);

        if (empty($anomalias)) {
            Flash::error('Anomalias not found');

            return redirect(route('anomaliases.index'));
        }

        return view('anomaliases.show')->with('anomalias', $anomalias);
    }

    /**
     * Show the form for editing the specified Anomalias.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $anomalias = $this->anomaliasRepository->findWithoutFail($id);

        if (empty($anomalias)) {
            Flash::error('Anomalias not found');

            return redirect(route('anomaliases.index'));
        }

        return view('anomaliases.edit')->with('anomalias', $anomalias);
    }

    /**
     * Update the specified Anomalias in storage.
     *
     * @param  int              $id
     * @param UpdateAnomaliasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAnomaliasRequest $request)
    {
        $anomalias = $this->anomaliasRepository->findWithoutFail($id);

        if (empty($anomalias)) {
            Flash::error('Anomalias not found');

            return redirect(route('anomaliases.index'));
        }
        if (!empty(Anomalias::where('codigo', $request->get('codigo'))->first())) {
            $anomalias = $this->anomaliasRepository->update($request->all(), $id);
            Flash::success('Anomalias updated successfully.');
        }else{
            Flash::success('Anomalia no guardada.');
        }



        return redirect(route('anomaliases.index'));
    }

    /**
     * Remove the specified Anomalias from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $anomalias = $this->anomaliasRepository->findWithoutFail($id);

        if (empty($anomalias)) {
            Flash::error('Anomalias not found');

            return redirect(route('anomaliases.index'));
        }

        $this->anomaliasRepository->delete($id);

        Flash::success('Anomalias deleted successfully.');

        return redirect(route('anomaliases.index'));
    }
}
