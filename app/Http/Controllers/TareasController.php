<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTareasRequest;
use App\Http\Requests\UpdateTareasRequest;
use App\Models\Flotillas;
use App\Models\InputFormatRow;
use App\Models\InputFormats;
use App\Models\Rutas;
use App\Models\Task;
use App\Models\TaskParams;
use App\Models\TaskTypes;
use App\Repositories\TareasRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class TareasController extends AppBaseController
{
    /** @var  TareasRepository */
    private $tareasRepository;

    public function __construct(TareasRepository $tareasRepo)
    {
        $this->tareasRepository = $tareasRepo;
    }

    /**
     * Display a listing of the Tareas.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, $status = 3, $lotecod = 'all')
    {
        $this->tareasRepository->pushCriteria(new RequestCriteria($request));
        //$tareas = $this->tareasRepository->all();
        $tareas = Task::where('id', '!=', 0);
        if($status != 3){
            $tareas->where('task_state', $status);
        }
        if($lotecod != 'all'){
            $tareas->where('task_observations', $lotecod);
        }
        $lotes = Task::select('task_observations')->distinct('task_observations')->pluck('task_observations')->toArray();

        $rutas = Rutas::get();
        $fecha = Carbon::now('America/Buenos_Aires')->format('Y-m-d');
        $flotillas = Flotillas::whereDate('hasta', ">=" , $fecha)->get();
       // dd($status);
        return view('tareas.index')->with('tareas',  $tareas->get())
                                    ->with('rutas',$rutas)
                                    ->with('flotillas',$flotillas)
                                    ->with('lotes',$lotes)
                                    ->with('status',$status)
            ->with('fechaDesde',null)
            ->with('fechaHasta',null);
    }

    public function buscarTareas(Request $request)
    {
        $tareas = Task::select('*');
        if(!empty($request->get('from'))){
            $tareas->whereBetween('fecha',[$request->get('from'),$request->get('to')]);
        }else{
            if(!empty($_SESSION['filters']['fecha_desde'])){
                $tareas->whereBetween('fecha',[
                    Carbon::createFromFormat('d/m/Y',$_SESSION['filters']['fecha_desde'])->format('Y-m-d'),
                    Carbon::createFromFormat('d/m/Y',$_SESSION['filters']['fecha_hasta'])->format('Y-m-d')
                ]);
            }
        }
        if(!empty($request->get('direccion'))){
            $tareas->where('task_street','like','%'.$request->get('direccion').'%');
        }else{
            if(!empty($_SESSION['filters']['direccion'])){
                $tareas->where('task_street','like','%'.$_SESSION['filters']['direccion'].'%');
            }
        }

        if(!empty($request->get('ruta'))){
            $tareas->where('lote',$request->get('ruta'));
        }else{
            if(!empty($_SESSION['filters']['ruta'])){
                $tareas->where('lote',$_SESSION['filters']['ruta']);
            }
        }
        if(!empty($request->get('clase'))){
            $tareas->where('dist_gest',$request->get('clase'));
        }else{
            if(!empty($_SESSION['filters']['clase'])){
                $tareas->where('dist_gest',$_SESSION['filters']['clase']);
            }
        }
        if(!empty($request->get('cliente'))){
            $tareas->where('expediente',$request->get('cliente'));
        }else{
            if(!empty($_SESSION['filters']['cliente'])){
                $tareas->where('expediente',$_SESSION['filters']['cliente']);
            }
        }
        if(!empty($request->get('medidor'))){
            $tareas->where('serie',$request->get('medidor'));
        }else{
            if(!empty($_SESSION['filters']['serie'])){
                $tareas->where('serie',$_SESSION['filters']['serie']);
            }
        }
        $rutas = Rutas::get();
        $flotillas = Flotillas::get();

        if(empty($request->get('from'))){
            $from = Carbon::now()->subMonth()->format('Y-m-d');
            $to = Carbon::now()->format('Y-m-d');
        }else{
            $from = $request->get('from');
            $to = $request->get('to');
        }
        $_SESSION['filters'] = [
            'fecha_desde'=>Carbon::createFromFormat('Y-m-d',$from)->format('d/m/Y'),
            'fecha_hasta'=>Carbon::createFromFormat('Y-m-d',$to)->format('d/m/Y'),
            'direccion'=>$request->get('direccion'),
            'ruta'=>$request->get('ruta'),
            'clase'=>$request->get('clase'),
            'cliente'=>$request->get('cliente'),
            'serie'=>$request->get('medidor')];

        return view('tareas.index')
            ->with('tareas', $tareas->get())
            ->with('rutas',$rutas)
            ->with('flotillas',$flotillas)
            ->with('fechaDesde',Carbon::createFromFormat('Y-m-d',$from)->format('d/m/Y'))
            ->with('fechaHasta',Carbon::createFromFormat('Y-m-d',$to)->format('d/m/Y'))
            ->with('direccion',$request->get('direccion'))
            ->with('ruta',$request->get('ruta'))
            ->with('clase',$request->get('clase'))
            ->with('cliente',$request->get('cliente'))
            ->with('serie',$request->get('medidor'));
    }
    /**
     * Show the form for creating a new Tareas.
     *
     * @return Response
     */
    public function create()
    {
        return view('tareas.create');
    }

    /**
     * Store a newly created Tareas in storage.
     *
     * @param CreateTareasRequest $request
     *
     * @return Response
     */
    public function store(CreateTareasRequest $request)
    {
        $input = $request->all();

        $tareas = $this->tareasRepository->create($input);

        Flash::success('Tareas saved successfully.');

        return redirect(route('tareas.index'));
    }

    /**
     * Display the specified Tareas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tareas = $this->tareasRepository->findWithoutFail($id);

        if (empty($tareas)) {
            Flash::error('Tareas not found');

            return redirect(route('tareas.index'));
        }

        return view('tareas.show')->with('tareas', $tareas);
    }

    /**
     * Show the form for editing the specified Tareas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tareas = $this->tareasRepository->findWithoutFail($id);

        if (empty($tareas)) {
            Flash::error('Tareas not found');

            return redirect(route('tareas.index'));
        }

        return view('tareas.edit')->with('tareas', $tareas);
    }

    /**
     * Update the specified Tareas in storage.
     *
     * @param  int              $id
     * @param UpdateTareasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTareasRequest $request)
    {
        $tareas = $this->tareasRepository->findWithoutFail($id);

        if (empty($tareas)) {
            Flash::error('Tareas not found');

            return redirect(route('tareas.index'));
        }

        $tareas = $this->tareasRepository->update($request->all(), $id);

        Flash::success('Tareas updated successfully.');

        return redirect(route('tareas.index'));
    }

    /**
     * Remove the specified Tareas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tareas = $this->tareasRepository->findWithoutFail($id);

        if (empty($tareas)) {
            Flash::error('Tareas not found');

            return redirect(route('tareas.index'));
        }

        $this->tareasRepository->delete($id);

        Flash::success('Tareas deleted successfully.');

        return redirect(route('tareas.index'));
    }
    public function loadTasks(){
        $from = Carbon::now()->format('d/m/Y');
        return view('tareas.newRuta')->with('from',$from)->with('tareas',[]);
    }
    public function saveRuta2(Request $request){
        $archivo = $request->file('ruta');
        Excel::load($archivo->getRealPath(), function ($reader) use ($request) {
            $sheet = $reader->toArray();
            foreach ( array_filter($sheet) as $key => $row) {

                $importFormat = InputFormats::find($request->get('tipo'));
                $importFormatFields = InputFormatRow::where('importFormat',$request->get('tipo'))->orderBy('orden')->get();

                foreach ($importFormatFields as $llaves => $fields) {
                    $fieldname = Task::translateColumn($fields->name);
                    if ($fieldname == 'taskType') {
                        $task->task_type = TaskTypes::where('alias', $fields->value)->first()->id; //esto depende de un task type hay que validar que exista un taskType
                    } else {
                        if (Schema::hasColumn('tasks', $fieldname)) {
                            if(!empty($row[$llaves][$fields->value])){
                                $value = $row[$llaves][$fields->value];
                                $task->{$fieldname} = $value;
                            }
                        }
                    }
                }
                $task->task_ot = uniqid();
                $task->task_urgency = '0';
                $task->fecha = Carbon::now()->format('Y-m-d');

                $task->save();
                
                foreach ($importFormatFields as $llave=> $fields) {
                    
                    $fieldname = Task::translateColumn($fields->name);
                    if (!Schema::hasColumn('tasks', $fieldname) && $fieldname != 'taskType') {
                        $direccParams = new TaskParams();
                        $direccParams->columna = $fieldname;
                        $direccParams->valor = $row[$llave][$fields->value];
                        $direccParams->task = $task->id;
                        $direccParams->save();
                    }
                }

                $localAddress = $this->getGeoCode($task->task_street, $task->task_street_number,$task->task_localidad);
                $task->task_latitude = (!empty($localAddress['latitude']) ? $localAddress['latitude'] : '');
                $task->task_longitude = (!empty($localAddress['longitude']) ? $localAddress['longitude'] : '');
                $task->save();
//                    $task = new Task();
//                    $task->task_ot = $row['secosot'];
//                    $task->task_type = $row['tipoosot'];
            }
        });
        return redirect('/modificarRutas');
    }
    public function saveRuta(Request $request){
     $archivo = $request->file('ruta');
     set_time_limit(1800);
        Excel::load($archivo->getRealPath(), function ($reader) use ($request) {
            $sheet = $reader->toArray();
            $importfieldsDB = InputFormatRow::where('importFormat',$request->get('tipo'))->orderBy('orden')->pluck('name', 'value')->toArray();

            foreach ( array_filter($sheet) as $key => $row) {
                $task = new Task();
                foreach ($row as $column => $value) {
                    $fieldname = Task::translateColumn( array_key_exists($column, $importfieldsDB)?$importfieldsDB[$column]:$column);

                    //dump($column);

                    if (Schema::hasColumn('tasks', $fieldname) && $fieldname != 'taskType' && $fieldname != 'servicios') {
                        $task->{$fieldname} = $value;
                    }
                    if ($fieldname == 'taskType') {
                        $task->task_type = TaskTypes::where('alias', $value)->first()->id; //esto depende de un task type hay que validar que exista un taskType
                    }

                }
               // dd(123);
                $task->task_ot = uniqid();
                $task->task_urgency = '0';
                $task->fecha = Carbon::now()->format('Y-m-d');
                $task->save();
                foreach ($row as $column => $value) {
                    $fieldname = Task::translateColumn(array_key_exists($column, $importfieldsDB)?$importfieldsDB[$column]:$column);
                    //dump($column);
                    if (!Schema::hasColumn('tasks', $fieldname) && $fieldname != 'taskType') {
                        $direccParams = new TaskParams();
                        $direccParams->columna = $fieldname;
                        $direccParams->valor = $value;
                        $direccParams->task = $task->id;
                        $direccParams->save();
                    }
                }
                $localAddress = $this->getGeoCode($task->task_street, $task->task_street_number,$task->task_localidad);
                $task->task_latitude = (!empty($localAddress['latitude']) ? $localAddress['latitude'] : '');
                $task->task_longitude = (!empty($localAddress['longitude']) ? $localAddress['longitude'] : '');
                $task->save();
            }
        });

        return redirect('/tareas');
    }
    private function getGeoCode($calle,$altura,$localidad){
//        $localize = file_get_contents();
//        $key = 'AIzaSyCXzgMgeX0DrtYECOtubJls3laXBFTvtMQ';
        $key = 'AIzaSyAnm6COKIXO-91niHCRMbE7NPeTCe-OACE';

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($calle).'+'.$altura.','.urlencode($localidad).',Buenos+aires,Argentina&sensor=true&key='.$key;
//            echo $url;
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $data = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        $data = json_decode($data);
        if(empty($data->results) || empty($data->results[0]->address_components[3])){
            return ['calle'=>$calle,'localidad'=>$localidad,'latitude'=>'','longitude'=>''];
        }

        if($localidad == 'CIUDAD BS AS' && !empty($data->results[0]->address_components[3])) {
            if ($data->results[0]->address_components[3]->types[0] == 'administrative_area_level_2') {
                $localidad =  $data->results[0]->address_components[3]->long_name;
                return ['calle'=>$data->results[0]->address_components[1]->long_name,'localidad'=>$this->quitar_tildes($localidad),'latitude'=>$data->results[0]->geometry->location->lat,'longitude'=>$data->results[0]->geometry->location->lng];
            }
        }

        if(!in_array('sublocality', $data->results[0]->address_components[2]->types)){
            $localidad = $data->results[0]->address_components[2]->long_name;
        }
        return ['calle'=>$data->results[0]->address_components[1]->long_name,'localidad'=>$this->quitar_tildes($localidad),'latitude'=>$data->results[0]->geometry->location->lat,'longitude'=>$data->results[0]->geometry->location->lng];

    }
    private function quitar_tildes($cadena) {
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $texto = str_replace($no_permitidas, $permitidas ,$cadena);
        return $texto;
    }
    public function modificarRutas(){
        $day = Carbon::now()->format('Y-m-d');
        $tareas = Task::where('fecha',$day)->get();
        $rutas = Rutas::where('fecha',$day)->orderBy('ruta_id')->get();
        $flotillas = Flotillas::whereDate('desde','<=',$day)->whereDate('hasta','>=',$day)->get();//Aqui se buscan las flotillas disponibles en el dia?
        //dd($tareas);
        foreach($flotillas as $k=>$flotilla) {
          //  dump($flotilla);
            $newRuta = Rutas::where('user_id',$flotilla->id)->where('fecha',$day)->first();
            if (empty($newRuta) || !$newRuta) {
                $newRuta = new Rutas();
                $newRuta->fecha = $day;
                $newRuta->user_id = $flotilla->id;
                $newRuta->order = $k;
                $newRuta->task_id = 0;
                $newRuta->ruta_id = ($k + 1); //Aqui siempre se tendra un numero repetitivo no preciso ni relacional
                $newRuta->save();
            }
        }
        return view('tareas.mapaGps')->with('rutas',$rutas)->with('entrevistas',$tareas)->with('day',$day)->with('franjas',[])->with('flotillas',$flotillas);
    }
    public function saveRutas(Request $request){
//        sleep(rand(0,10));
        foreach($request->get('order') as $order=>$elem){
            $ruta_id = str_replace("ruta_","",$elem['id']);
            if($ruta_id != 0){
                $task_id = $elem['ruta'];
                $ruta = Rutas::where('task_id',$task_id)->first();
                if(empty($ruta)){
                    $ruta = new Rutas();
                }
                $ruta->fecha = $request->get('dia');
                $ruta->order = $order;
                $ruta->ruta_id = $ruta_id;
                $ruta->task_id = $task_id;
//                $ruta->user_id = $
                $ruta->save();
            }

        }
    }
    public function guardarRutasPoly(Request $request){
        $ruta = Rutas::where('user_id',$request->get('flotilla'))->first();
        //dd($ruta);
        if(empty($ruta)){
            $ruta = new Rutas();
            $ruta->user_id = $request->get('flotilla');
            $ruta->order = 0;
            $ruta->ruta_id = rand();
            $ruta->fecha = $request->get('fecha');
            $ruta->task_id = 0;            
            //dump(1);
            $ruta->save();
            //dump(2);
        }
        $rutaSelected = $ruta->ruta_id;
        foreach($request->get('direccion') as $elem){
            $rutaIf = Rutas::where('task_id',$elem)->first();

            Task::where('id', $elem)->update(['task_state' => 1]); // agregar esto para cambiar el status de la task

            if(empty($rutaIf)){
                $rutaIf = new Rutas();
                $rutaIf->order = rand();
                $rutaIf->fecha = $request->get('fecha');
                $rutaIf->task_id = $elem;
            }
            $rutaIf->ruta_id = $rutaSelected;
            $rutaIf->save();
        }
        echo '1';
    }
    public function newGeo(Request $request){
        $data = $request->all();
        $task = Task::find($data['item']);
        $task->task_latitude = $data['lat'];
        $task->task_longitude = $data['long'];
        $task->save();
        echo 1;
    }
    public function imprimirTarea($id){
        $tareas = $this->tareasRepository->findWithoutFail($id);

        if (empty($tareas)) {
            Flash::error('Tareas not found');

            return redirect(route('tareas.index'));
        }
        return view('tareas.print')->with('tareas', $tareas)->with('printView',true);
    }
}
