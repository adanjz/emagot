<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubTasksRequest;
use App\Http\Requests\UpdateSubTasksRequest;
use App\Repositories\SubTasksRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SubTasksController extends AppBaseController
{
    /** @var  SubTasksRepository */
    private $subTasksRepository;

    public function __construct(SubTasksRepository $subTasksRepo)
    {
        $this->subTasksRepository = $subTasksRepo;
    }

    /**
     * Display a listing of the SubTasks.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->subTasksRepository->pushCriteria(new RequestCriteria($request));
        $subTasks = $this->subTasksRepository->all();

        return view('sub_tasks.index')
            ->with('subTasks', $subTasks);
    }

    /**
     * Show the form for creating a new SubTasks.
     *
     * @return Response
     */
    public function create()
    {
        return view('sub_tasks.create');
    }

    /**
     * Store a newly created SubTasks in storage.
     *
     * @param CreateSubTasksRequest $request
     *
     * @return Response
     */
    public function store(CreateSubTasksRequest $request)
    {
        $input = $request->all();

        $subTasks = $this->subTasksRepository->create($input);

        Flash::success('Sub Tasks saved successfully.');

        return redirect(route('subTasks.index'));
    }

    /**
     * Display the specified SubTasks.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subTasks = $this->subTasksRepository->findWithoutFail($id);

        if (empty($subTasks)) {
            Flash::error('Sub Tasks not found');

            return redirect(route('subTasks.index'));
        }

        return view('sub_tasks.show')->with('subTasks', $subTasks);
    }

    /**
     * Show the form for editing the specified SubTasks.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subTasks = $this->subTasksRepository->findWithoutFail($id);

        if (empty($subTasks)) {
            Flash::error('Sub Tasks not found');

            return redirect(route('subTasks.index'));
        }

        return view('sub_tasks.edit')->with('subTasks', $subTasks);
    }

    /**
     * Update the specified SubTasks in storage.
     *
     * @param  int              $id
     * @param UpdateSubTasksRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubTasksRequest $request)
    {
        $subTasks = $this->subTasksRepository->findWithoutFail($id);

        if (empty($subTasks)) {
            Flash::error('Sub Tasks not found');

            return redirect(route('subTasks.index'));
        }

        $subTasks = $this->subTasksRepository->update($request->all(), $id);

        Flash::success('Sub Tasks updated successfully.');

        return redirect(route('subTasks.index'));
    }

    /**
     * Remove the specified SubTasks from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subTasks = $this->subTasksRepository->findWithoutFail($id);

        if (empty($subTasks)) {
            Flash::error('Sub Tasks not found');

            return redirect(route('subTasks.index'));
        }

        $this->subTasksRepository->delete($id);

        Flash::success('Sub Tasks deleted successfully.');

        return redirect(route('subTasks.index'));
    }
}
