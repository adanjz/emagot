<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInputFormatsRequest;
use App\Http\Requests\UpdateInputFormatsRequest;
use App\Models\InputFormatRow;
use App\Repositories\InputFormatsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class InputFormatsController extends AppBaseController
{
    /** @var  InputFormatsRepository */
    private $inputFormatsRepository;

    public function __construct(InputFormatsRepository $inputFormatsRepo)
    {
        $this->inputFormatsRepository = $inputFormatsRepo;
        $this->campos = ['titulo','descripcion','calle','altura','piso','dpto','localidad','servicio','parametro_servicio','task_op','dist_gest','lote','fecha_generacion','expediente','fecha_ingreso','ruta','serie','marca', 'task_observations'];

    }

    /**
     * Display a listing of the InputFormats.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inputFormatsRepository->pushCriteria(new RequestCriteria($request));
        $inputFormats = $this->inputFormatsRepository->all();

        return view('input_formats.index')
            ->with('inputFormats', $inputFormats);
    }

    /**
     * Show the form for creating a new InputFormats.
     *
     * @return Response
     */
    public function create()
    {
        return view('input_formats.create')->with('campos',$this->campos);
    }

    /**
     * Store a newly created InputFormats in storage.
     *
     * @param CreateInputFormatsRequest $request
     *
     * @return Response
     */
    public function store(CreateInputFormatsRequest $request)
    {
        $input = $request->all();


        $inputFormats = $this->inputFormatsRepository->create($input);
        $k = 0;
        foreach($input['campo'] as $key=>$campo){
            if(!empty($campo)){
                $inputFormatsField = InputFormatRow::where('importFormat',$inputFormats->id)
                    ->where('name',$input['key'][$key])
                    ->where('value',$campo)->first();
                if(empty($inputFormatsField)){
                    $inputFormatsField = new InputFormatRow();
                    $inputFormatsField->importFormat = $inputFormats->id;
                    $inputFormatsField->name = $input['key'][$key];
                    $inputFormatsField->value = $campo;
                    $inputFormatsField->orden = $k;
                    $inputFormatsField->save();
                }

                $k++;
            }

        }

        Flash::success('Input Formats saved successfully.');

        return redirect(route('inputFormats.index'));
    }

    /**
     * Display the specified InputFormats.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inputFormats = $this->inputFormatsRepository->findWithoutFail($id);

        if (empty($inputFormats)) {
            Flash::error('Input Formats not found');

            return redirect(route('inputFormats.index'));
        }

        return view('input_formats.show')->with('inputFormats', $inputFormats);
    }

    /**
     * Show the form for editing the specified InputFormats.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inputFormats = $this->inputFormatsRepository->findWithoutFail($id);

        if (empty($inputFormats)) {
            Flash::error('Input Formats not found');

            return redirect(route('inputFormats.index'));
        }

        return view('input_formats.edit')->with('inputFormats', $inputFormats)->with('campos',$this->campos);
    }

    /**
     * Update the specified InputFormats in storage.
     *
     * @param  int              $id
     * @param UpdateInputFormatsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInputFormatsRequest $request)
    {
        $inputFormats = $this->inputFormatsRepository->findWithoutFail($id);

        if (empty($inputFormats)) {
            Flash::error('Input Formats not found');

            return redirect(route('inputFormats.index'));
        }

        $inputFormats = $this->inputFormatsRepository->update($request->all(), $id);
        $input = $request->all();
        $k = 0;
        foreach($input['campo'] as $key=>$campo){
            if(!empty($campo)){
                $campoT = InputFormatRow::translateColumn($input['key'][$key]);
                $inputFormatsField = InputFormatRow::where('importFormat',$inputFormats->id)->where('name',$campoT)->first();
                if(empty($inputFormatsField)) {
                    $inputFormatsField = InputFormatRow::where('importFormat', $inputFormats->id)->where('name',
                        $input['key'][$key])->first();
                    if (empty($inputFormatsField)) {
                        $inputFormatsField = new InputFormatRow();
                    }
                }

                $inputFormatsField->importFormat = $inputFormats->id;
                $inputFormatsField->name = $input['key'][$key];
                $inputFormatsField->value = $campo;
                $inputFormatsField->orden = $k;
                $inputFormatsField->save();
                $k++;
            }
        }
        $deleted =$request->get('deletedFields');
        $del = explode(',',$deleted);
        foreach($del as $d){
            $inputFormats = InputFormatRow::find($d);
            if(!empty($inputFormats)){
                $inputFormats->delete();
            }
        }

        Flash::success('Input Formats updated successfully.');

        return redirect(route('inputFormats.index'));
    }

    /**
     * Remove the specified InputFormats from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inputFormats = $this->inputFormatsRepository->findWithoutFail($id);

        if (empty($inputFormats)) {
            Flash::error('Input Formats not found');

            return redirect(route('inputFormats.index'));
        }

        $this->inputFormatsRepository->delete($id);

        Flash::success('Input Formats deleted successfully.');

        return redirect(route('inputFormats.index'));
    }
}
