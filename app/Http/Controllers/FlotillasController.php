<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFlotillasRequest;
use App\Http\Requests\UpdateFlotillasRequest;
use App\Models\FlotillasUsuarios;
use App\Models\LegajoGps;
use App\Models\Legajos;
use App\Models\User;
use App\Repositories\FlotillasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class FlotillasController extends AppBaseController
{
    /** @var  FlotillasRepository */
    private $flotillasRepository;

    public function __construct(FlotillasRepository $flotillasRepo)
    {
        $this->flotillasRepository = $flotillasRepo;
    }

    /**
     * Display a listing of the Flotillas.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->flotillasRepository->pushCriteria(new RequestCriteria($request));
        $flotillas = $this->flotillasRepository->all();

        return view('flotillas.index')
            ->with('flotillas', $flotillas);
    }

    /**
     * Show the form for creating a new Flotillas.
     *
     * @return Response
     */
    public function create()
    {
        return view('flotillas.create')->with('selectedUsuarios',[]);
    }

    /**
     * Store a newly created Flotillas in storage.
     *
     * @param CreateFlotillasRequest $request
     *
     * @return Response
     */
    public function store(CreateFlotillasRequest $request)
    {
        $input = $request->all();

        $flotillas = $this->flotillasRepository->create($input);

        foreach($input['usuarios'] as $usuario){
            $flotillaUsuario = new FlotillasUsuarios();
            $flotillaUsuario->flotilla = $flotillas->id;
            $flotillaUsuario->usuario = $usuario;
            $flotillaUsuario->save();
        }
        Flash::success('Flotillas saved successfully.');

        return redirect(route('flotillas.index'));
    }

    /**
     * Display the specified Flotillas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $flotillas = $this->flotillasRepository->findWithoutFail($id);

        if (empty($flotillas)) {
            Flash::error('Flotillas not found');

            return redirect(route('flotillas.index'));
        }

        return view('flotillas.show')->with('flotillas', $flotillas);
    }

    /**
     * Show the form for editing the specified Flotillas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $flotillas = $this->flotillasRepository->findWithoutFail($id);

        if (empty($flotillas)) {
            Flash::error('Flotillas not found');

            return redirect(route('flotillas.index'));
        }

        $fus = $flotillas->flotillasUsuarios;
        foreach($fus as $fu){
            $selectedUsuarios[] = $fu->usuario;
        }
        return view('flotillas.edit')->with('flotillas', $flotillas)->with('selectedUsuarios',$selectedUsuarios);
    }

    /**
     * Update the specified Flotillas in storage.
     *
     * @param  int              $id
     * @param UpdateFlotillasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFlotillasRequest $request)
    {
        $flotillas = $this->flotillasRepository->findWithoutFail($id);

        if (empty($flotillas)) {
            Flash::error('Flotillas not found');

            return redirect(route('flotillas.index'));
        }

        $flotillas = $this->flotillasRepository->update($request->all(), $id);
        $flotillas->flotillasUsuarios()->delete();
        foreach($request->get('usuarios')as $usuario){
            $flotillaUsuario = new FlotillasUsuarios();
            $flotillaUsuario->flotilla = $flotillas->id;
            $flotillaUsuario->usuario = $usuario;
            $flotillaUsuario->save();
        }
        Flash::success('Flotillas updated successfully.');

        return redirect(route('flotillas.index'));
    }

    /**
     * Remove the specified Flotillas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $flotillas = $this->flotillasRepository->findWithoutFail($id);

        if (empty($flotillas)) {
            Flash::error('Flotillas not found');

            return redirect(route('flotillas.index'));
        }

        $this->flotillasRepository->delete($id);

        Flash::success('Flotillas deleted successfully.');

        return redirect(route('flotillas.index'));
    }

    public function seguimientoGps(){
        return view('legajo_gps.index')->with('legajoGps',User::get());
    }
    public function seguir($id){
        return view('legajo_gps.show')->with('legajo',User::find($id))->with('gps',LegajoGps::where('legajo_id',$id)->orderBy('id','desc')->first());
    }
}
