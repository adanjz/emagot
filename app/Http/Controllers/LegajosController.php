<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLegajosRequest;
use App\Http\Requests\UpdateLegajosRequest;
use App\Repositories\LegajosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class LegajosController extends AppBaseController
{
    /** @var  LegajosRepository */
    private $legajosRepository;

    public function __construct(LegajosRepository $legajosRepo)
    {
        $this->legajosRepository = $legajosRepo;
    }

    /**
     * Display a listing of the Legajos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->legajosRepository->pushCriteria(new RequestCriteria($request));
        $legajos = $this->legajosRepository->all();

        return view('legajos.index')
            ->with('legajos', $legajos);
    }

    /**
     * Show the form for creating a new Legajos.
     *
     * @return Response
     */
    public function create()
    {
        return view('legajos.create');
    }

    /**
     * Store a newly created Legajos in storage.
     *
     * @param CreateLegajosRequest $request
     *
     * @return Response
     */
    public function store(CreateLegajosRequest $request)
    {
        $input = $request->all();

        $legajos = $this->legajosRepository->create($input);

        Flash::success('Legajos saved successfully.');

        return redirect(route('legajos.index'));
    }

    /**
     * Display the specified Legajos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $legajos = $this->legajosRepository->findWithoutFail($id);

        if (empty($legajos)) {
            Flash::error('Legajos not found');

            return redirect(route('legajos.index'));
        }

        return view('legajos.show')->with('legajos', $legajos);
    }

    /**
     * Show the form for editing the specified Legajos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $legajos = $this->legajosRepository->findWithoutFail($id);

        if (empty($legajos)) {
            Flash::error('Legajos not found');

            return redirect(route('legajos.index'));
        }

        return view('legajos.edit')->with('legajos', $legajos);
    }

    /**
     * Update the specified Legajos in storage.
     *
     * @param  int              $id
     * @param UpdateLegajosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLegajosRequest $request)
    {
        $legajos = $this->legajosRepository->findWithoutFail($id);

        if (empty($legajos)) {
            Flash::error('Legajos not found');

            return redirect(route('legajos.index'));
        }

        $legajos = $this->legajosRepository->update($request->all(), $id);

        Flash::success('Legajos updated successfully.');

        return redirect(route('legajos.index'));
    }

    /**
     * Remove the specified Legajos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $legajos = $this->legajosRepository->findWithoutFail($id);

        if (empty($legajos)) {
            Flash::error('Legajos not found');

            return redirect(route('legajos.index'));
        }

        $this->legajosRepository->delete($id);

        Flash::success('Legajos deleted successfully.');

        return redirect(route('legajos.index'));
    }
}
