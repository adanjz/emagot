<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskTypesRequest;
use App\Http\Requests\UpdateTaskTypesRequest;
use App\Models\SubTasks;
use App\Models\TaskTypes;
use App\Repositories\TaskTypesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TaskTypesController extends AppBaseController
{
    /** @var  TaskTypesRepository */
    private $taskTypesRepository;

    public function __construct(TaskTypesRepository $taskTypesRepo)
    {
        $this->taskTypesRepository = $taskTypesRepo;
    }

    /**
     * Display a listing of the TaskTypes.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->taskTypesRepository->pushCriteria(new RequestCriteria($request));
        $taskTypes = $this->taskTypesRepository->all();

        return view('task_types.index')
            ->with('taskTypes', $taskTypes);
    }

    /**
     * Show the form for creating a new TaskTypes.
     *
     * @return Response
     */
    public function create()
    {
        return view('task_types.create');
    }

    /**
     * Store a newly created TaskTypes in storage.
     *
     * @param CreateTaskTypesRequest $request
     *
     * @return Response
     */
    public function store(CreateTaskTypesRequest $request)
    {
        $input = $request->all();


        $taskTypes = new TaskTypes();
        $taskTypes->name = $input['name'];
        $taskTypes->alias = $input['alias'];
        $taskTypes->cliente = $input['cliente'];
        $taskTypes->precio = $input['precio'];
        $taskTypes->max_time = $input['max_time'];
        $taskTypes->priority = $input['priority'];
        $taskTypes->workersRequired = $input['workersRequired'];
        $taskTypes->save();
        foreach($input['subtasks'] as $subtask){
            $newSubTask = new SubTasks();
            $newSubTask->name = $subtask['nombre'];
            $newSubTask->max_time = $subtask['max_time'];
            $newSubTask->precio = $subtask['precio'];
            $newSubTask->alias = $subtask['alias'];
            $newSubTask->workersRequired = $subtask['workersRequired'];
            $newSubTask->taskType = $taskTypes->id;
            $newSubTask->save();
        }

        Flash::success('Task Types saved successfully.');

        return redirect(route('taskTypes.index'));
    }

    /**
     * Display the specified TaskTypes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $taskTypes = $this->taskTypesRepository->findWithoutFail($id);

        if (empty($taskTypes)) {
            Flash::error('Task Types not found');

            return redirect(route('taskTypes.index'));
        }

        return view('task_types.show')->with('taskTypes', $taskTypes);
    }

    /**
     * Show the form for editing the specified TaskTypes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $taskTypes = $this->taskTypesRepository->findWithoutFail($id);

        if (empty($taskTypes)) {
            Flash::error('Task Types not found');

            return redirect(route('taskTypes.index'));
        }

        return view('task_types.edit')->with('taskTypes', $taskTypes);
    }

    /**
     * Update the specified TaskTypes in storage.
     *
     * @param  int              $id
     * @param UpdateTaskTypesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTaskTypesRequest $request)
    {
        $taskTypes = $this->taskTypesRepository->findWithoutFail($id);

        $input = $request->all();
        if (empty($taskTypes)) {
            Flash::error('Task Types not found');

            return redirect(route('taskTypes.index'));
        }

//        $taskTypes = $this->taskTypesRepository->update($request->all(), $id);

        $taskTypes->name = $input['name'];
        $taskTypes->alias = $input['alias'];
        $taskTypes->cliente = $input['cliente'];
        $taskTypes->precio = $input['precio'];
        $taskTypes->max_time = $input['max_time'];
        $taskTypes->priority = $input['priority'];
        $taskTypes->workersRequired = $input['workersRequired'];
        $taskTypes->save();
        $taskTypes->subTasks()->delete();
        foreach($input['subtasks'] as $subtask){
            $newSubTask = new SubTasks();
            $newSubTask->name = $subtask['nombre'];
            $newSubTask->max_time = $subtask['max_time'];
            $newSubTask->precio = $subtask['precio'];
            $newSubTask->alias = $subtask['alias'];
            $newSubTask->workersRequired = $subtask['workersRequired'];
            $newSubTask->taskType = $taskTypes->id;
            $newSubTask->save();
        }

        Flash::success('Task Types updated successfully.');

        return redirect(route('taskTypes.index'));
    }

    /**
     * Remove the specified TaskTypes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $taskTypes = $this->taskTypesRepository->findWithoutFail($id);

        if (empty($taskTypes)) {
            Flash::error('Task Types not found');

            return redirect(route('taskTypes.index'));
        }

        $this->taskTypesRepository->delete($id);

        Flash::success('Task Types deleted successfully.');

        return redirect(route('taskTypes.index'));
    }
}
